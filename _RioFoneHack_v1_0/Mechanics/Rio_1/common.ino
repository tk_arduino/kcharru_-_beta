#include "Arduino.h"

// Volume
uint8_t initialVolume = 0;
uint8_t volumeValueLeft = initialVolume; // LOUT for Handset!!
uint8_t volumeValueRight = initialVolume; // LOUT for Speaker!!




// Ringing & Presence
bool isRinging;
int presenceCount = 0;
const unsigned long waitAfterRing = 20000;
//const unsigned long waitAfterEnd = 30000;
int timerSetDetect;
int timerStopRinging;
int timerRandom;

unsigned long distance; // in cm
bool isPresence = false;

// Colgar / Descolgar
elapsedMillis initColgar = 2000;
elapsedMillis initDescolgar = 2000;



// Test
int goToQuestion = false;
String specialCode;



/////////////////////////// COLGAR / DESCOLGAR => HangUp / PickUp //////////////////////
void colgadoStateChanged() {

  isColgado = digitalRead(interruptPin);
  if (isColgado == previousIsColgado) {
    //p(F("isColgado state has not changed. Do nothing"));
    return;
  }
  previousIsColgado = isColgado;

  if (isColgado) {
    colgar();
  }
  else {
    descolgar();
  }
}

void colgar() {
  if (initColgar > 1000) {
    p(F(" - HangUp. Done."));
    stopMechanic();
    initColgar = 0;
  }
  else {
    //p(F(" - DOES not HangUP"));
  }

}

void descolgar() {
  if (initDescolgar > 1000) {
    p(F("PickUp. Done"));
    startMechanic();
    initDescolgar = 0;
  }
  else {
    //p(F("Does NOT PickUp"));
  }
}



///////////////////////////// MECHANIC START  /  STOP
void startMechanic() {

  if (isPlayingMechanic) {
    //p(F(" - - isPlayingMechanic is True, do not start Again"));
    return;
  }
  p(F(" - - Starting Mechanic"));

  p(F("freeMemory()="));
  p(String(freeMemory()));

  // Clear Variables
  initVariables();
  setBacklight("on");
  setChannel("left");

  isPlayingMechanic = true;
  isRinging = false;
  detectPresence = false;

  simpleTimer.deleteTimer(timerRandom);
  
  startStats();
  
  nextQuestion();
}

void stopMechanic() {
  if (!isPlayingMechanic) {
    //p(F(" isPlayingMechanic = FALSE, do nothing"));
    return;
  }

  
  musicPlayer.stopPlaying();
  endStats();

  p(F("Mechanic End"));

  //p(F("reseting..."));
  resetFunc();
  
  return;
  ////////// THIS IS NOT NEEDED, SINCE ARDUINO IS RESTARTED

  // Leave this code here, just in case



  isPlayingMechanic = false;
  musicPlayer.softReset();
  musicPlayer.stopPlaying();

  // Start Timers for setDetect and RandomRing
  //simpleTimer.deleteTimer(timerSetDetect);
  timerSetDetect = simpleTimer.setTimeout(waitAfterEnd, setDetectPresence);
  //p(F("timerSetDetect id: " + String(timerSetDetect));
  timerRandom = simpleTimer.setTimeout(randomRingInterval, randomRing);

  setBacklight("off");
  initPlayer();
  
     
}





//////////////////////// KEYPAD  ////////////////////////
void readSerial() {
  if (Serial.available() > 0)
  {
    char c = Serial.read();
    keyReceived(c);
  }
}

void readKeypad() {
  char key = keypad.getKey();
  if (key) {
    keyReceived(key);
  }
}


//- HELPERS
void p(String text) {
  if(testMode){
    Serial.println(text);
  }
  
}



//////////////////////////////   KEYS    /////////////

void keyReceived(char c) {

  if (!testMode && elapsedQuestion < 2000) {
    p(F("Key  in less than 2 secods after question, ignore"));
    return;
  }

  if (fullPlaying) {
    p(F("playing full, do not answer to keypad"));
    // Dev
    if (testMode && c == '0') {
      nextQuestion();
    }
    return;
  }


  p(F("Key Received: ")); p(String(c));
  if (c == 'd') {
    descolgar();
  }

  if (c == 'c') {
    colgar();
  }

  if (testMode && c == 'C') {
    if (!goToQuestion) {
      goToQuestion = true;
      printLcd("C", 0, "both", true);
      return;
    }
    else {
      questionsCount = specialCode.toInt() - 1;
      specialCode = "";
      goToQuestion = false;
      nextQuestion();
      return;
    }

  }
  if (c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' ||
      c == '6' || c == '7' || c == '8' || c == '9' || c == '#' ||
      (c == '*' && questionsCount == 14)) {
    processChoice(c);
    return;
  }


  if (c == '*') {
    repeatQuestion();
  }

  // VOLUME
  if (c == 'A') {
    setVolumeUp();
  }
  if (c == 'B') {
    setVolumeDown();
  }
  if (c == 'D') {
    descolgar();
  }

  if (c == 'r') {
    startRinging(false);
  }
}


