#include <RioPhoneHack.h>
RioPhoneHack rio;

//- LCD   //////////////////////////////////////////////////////
#include <LiquidCrystal.h>
LiquidCrystal lcd(RS, EN, DB4, DB5, DB6, DB7);

// KEYPAD
#include <Keypad.h>
byte rowPins[ROWS] = {pinDown1, pinDown2, pinDown3, pinDown4}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {pinUp1, pinUp2, pinUp3, pinUp4}; //connecti to the column pinouts of the keypad
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

// PROX SENSOR
#include <NewPing.h>
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

// TIMER
#include "SimpleTimer.h"
SimpleTimer simpleTimer;

#include <elapsedMillis.h>

// AUDIO PLAYER
#include <SPI.h>
#include <Adafruit_VS1053.h>
#include <SD.h>
Adafruit_VS1053_FilePlayer musicPlayer = Adafruit_VS1053_FilePlayer(BREAKOUT_RESET, BREAKOUT_CS, BREAKOUT_DCS, DREQ, CARDCS);

#include <MemoryFree.h>

/////////////////////////// Config Test Values
bool testMode = true;
///////////////////////////

//////////////////////////// GLOBALS
// General Variables
int interruptPin = 19;
bool isColgado = true; 
bool previousIsColgado = true;
bool detectPresence = true;

String playingFileName;
String answerAudio;
String action;
String userInput;

bool isPlayingMechanic;
elapsedMillis elapsedQuestion;
unsigned long elapsedLimit = 2000;
bool fullPlaying;
const unsigned long randomRingInterval = 60000;

// Rio 1 Variables
const int rioNumber = 1;
const String folder = String(rioNumber) + "/RH" + String(rioNumber) + "_";
const int delayLoop = 10;

// Questions
const int numberOfQuestions = 25;
int questionsCount;
const int maxChoices = 3 - 1;
int choices[numberOfQuestions];

// Others
String age;
bool repeatingLastQuestion;
int waitingResponseTimer;
bool isRepeated;

const unsigned long waitAfterEnd = 30000;

// Init global variables
void initVariables(){
  
  playingFileName = "";
  answerAudio = "";
  action = "";
  userInput = "";
  
  isPlayingMechanic = false;
  elapsedQuestion = 0;
  fullPlaying = false;
  
  questionsCount = 0;
  //choices[numberOfQuestions];
  
  age = "";
  repeatingLastQuestion = false;
  isRepeated = false;
  
  // clearTimer
  simpleTimer.deleteTimer(waitingResponseTimer);
  
}

////////////////////////////////////////////////// QUESTIONS
String questions[numberOfQuestions + 1][3] = {
  {"nothingHere", "nothingAnswer", "0"},
  // 1 AGE
  {"notAnswer", "notAnswer", "notAnswer"},
  // 2
  {"002a.mp3", "002b.mp3", "002c.mp3"},
  // 3.   1 goes to 5th question
  {"003a.mp3", "003b.mp3", "003c.mp3"},
  // 4
  {"004a.mp3", "004b.mp3", "004c.mp3"},
  // 5 Introduction Quickfire Section
  {"notChoice", "notAnswer", "notAnswer"},
  // 6
  {"notPlay", "notAnswer", "notAllowed"},
  // 7
  {"notAnswer", "notAnswer", "notAllowed"},
  // 8
  {"notAnswer", "notAnswer", "notAllowed"},
  // 9
  {"notAnswer", "notAnswer", "notAllowed"},
  // 10
  {"notPlay", "notAnswer", "notAllowed"},
  // 11 RANDOM Drum Roll
  {"notAnswer", "notAnswer", "notAnswer"},
  // 12 Introduction to Phrase Repeat
  {"notChoice", "notAnswer", "notAnswer"},

  // 13   Conditionated 4a
  {"013a.mp3", "013b.mp3", "notAllowed"},
  // 14
  {"014a.mp3", "notAnswer", "notAllowed"},
  // 15
  {"015a.mp3", "notAllowed", "notAllowed"},

  // 16   Conditionated 4b
  {"016a.mp3", "016b.mp3", "notAllowed"},
  // 17
  {"017a.mp3", "notAnswer", "notAllowed"},
  // 18
  {"018a.mp3", "notAllowed", "notAllowed"},

  // 19   Conditionated 4c
  {"019a.mp3", "019b.mp3", "notAllowed"},
  // 20
  {"020a.mp3", "notAnswer", "notAllowed"},
  // 21
  {"021a.mp3", "notAllowed", "notAllowed"},

  // 22   WAITING for Conducttr  to call the player (60 secs)
  {"notAnswer", "notAnswer", "notAllowed"},
  // 23 RANDOM NAMES!
  {"notAnswer", "notAnswer", "notAnswer"},
  // 24 LAMBADA!
  {"notChoice", "notAnswer", "notAnswer"},
  // 25
  {"025a.mp3", "025b.mp3", "025a.mp3"}

};


///////////////////////////////////////////////////////////////      MECHANIC      /////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void nextQuestion() {
  p(F("----- En NextQuestion ----"));
  lcd.begin(16, 2);  //LCD
  questionsCount++;
  
  
  
  String type = questions[questionsCount][0];

  if (type == "notPlay") { // 6 and 10 shouldn't play
    //p(F("notPlay:  nextQuestion"));
    nextQuestion();
    return;
  }

  printQuestion(' ');

  // Special Cases
  if (questionsCount == 13) {

    int userChoice = choices[4];
    ////////////////////    ////////////////////    ////////////////////
    // For testing, if not choice in 4th question
    if (userChoice == 0) {
      userChoice = 1;
    }
    ////////////////////    ////////////////////    ////////////////////
    //p(F("userChoicein Question 4 ") + String(userChoice));
    if (userChoice != 1) {
      // 2 (b) goes to question 16.  3 (c) goes to 19
      questionsCount = questionsCount + ( (userChoice - 1) * 3);
    }
  }

  addToStats(" Q" + String(questionsCount));

  playingFileName = rio.getQuestionFileName(folder, questionsCount);

  if (type == "notChoice") {
    playAudio(playingFileName, true);
    return;
  }

  if (questionsCount == 15 || questionsCount == 18 || questionsCount == 21) {
    action = "waitingResponseEnd";
    playAudio(playingFileName, true);
    return;
  }

  playAudio(playingFileName, false);

  if (questionsCount == 23) {

    action = "processChoice1";
    playAudio(playingFileName, true);
    return;

  }
  if(questionsCount == 25){
    repeatingLastQuestion = false;
  }

  // Check END
  if (questionsCount > numberOfQuestions) {
    stopMechanic();
    return;
  }

}


void processChoice(char choice) {
  //p("ProcessChoice con choice " + String(choice) + " y question " + String(questionsCount));
  addToStats(" C" + String(choice));
  // Get choice
  // Do not beep for the next Questions, since they are processChoice forced
  if(questionsCount != 23 && questionsCount != 15 && questionsCount != 18 && questionsCount != 21){
    beep();
  }
  int convertedChoice = choice - '0';
  choices[questionsCount] = convertedChoice;
  convertedChoice--;

  if (questionsCount == 25) {
    // Here it changes questionsCount to 1 AND I dont find why! Just put it back to 25
    questionsCount = 25;
    
    if(repeatingLastQuestion && choice != '1') {
      return;
    }
  }
  
  // Get Type
  String type = questions[questionsCount][0];

  // Special cases
  if (questionsCount == 1) { // AGE
    if (choice != '#') {
      if (userInput.length() == 2) {
        //p(F("Age already has 2 digits"));
        return;
      }
      userInput = userInput + String(choice);
      String sufix = rio.getSufix(questionsCount, userInput.toInt());
      printLcd(userInput + sufix, 0, "both", false);
      return;
    }
    else {
      age = userInput;
      userInput = "";

      answerAudio = rio.getRio1Answer1(age.toInt());
      if (answerAudio == "OK") {
        nextQuestion();
        return;
      }
      else {
        playingFileName = folder + answerAudio;
        playAudio(playingFileName, true); // Special. Has to end
        printQuestion(choice);
        action = "stopMechanic";
        return;
      }
    }
  }
  
  // This is not executed for Age
  printQuestion(choice);

  if (questionsCount == 11) { // RANDOM
    answerAudio = rio.getRio1Answer11();
    playingFileName = folder + answerAudio;
    playAudio(playingFileName, true);
    return;
  }

  if (questionsCount == 23) { // RANDOM based on user choice 22
    int userChoice = choices[22];
    answerAudio = rio.getRio1Answer23(userChoice);

    playingFileName = folder + answerAudio;
    
    playAudio(playingFileName, true);
    
    // We need to repeat it here!
    printQuestion(choice);
  }


  if (convertedChoice > maxChoices) {
    //p("Not Allowed, maxChoices is " + String(maxChoices + 1));
    return;
  }

  answerAudio = questions[questionsCount][convertedChoice];

  if (answerAudio == "notAllowed") {
    //p(F("Key NOT ALLOWED! Try again"));
    return;
  }


  if (questionsCount == 23) { // RandomNames
    return;
  }

  bool full = true;
  playingFileName = folder + answerAudio;
  if (answerAudio == "025b.mp3") {
    action = "lastQuestionWrong"; // Do not goes to nextQuestion!!
    repeatingLastQuestion = true;
    full = false; // Need it, to get keypad actions
  }
  if (answerAudio == "025a.mp3") {
    action = "stopMechanic"; // Do not goes to nextQuestion!!
  }
  
  if (answerAudio != "notAnswer") {
    playAudio(playingFileName, full);
  }
  else {
    //p(F("notAnswer, llamo a NextQuestion desde aqui porque tiene Not Answer"));
    nextQuestion();
    return;
  }

  if ((questionsCount == 14 || questionsCount == 17 || questionsCount == 20 ||
       questionsCount == 15 || questionsCount == 18 || questionsCount == 21)
      && choice == '1') {
        //p(F("go to question 22"));
    questionsCount = 21;
  }

  //p("processChoice End with  question " + String(questionsCount));

}

void checkQuestions() {
  if (!isPlayingMechanic) {
    return;
  }
  // Just check if Full
  if (fullPlaying || action == "lastQuestionWrong") {
    if (action == "doNothing") {
      return;
    }
    if ((elapsedQuestion > 2000) && (!musicPlayer.playingMusic)) {
      elapsedQuestion = 0;
      p(" -- checkQuestions, audio end. questionsCount is " + String(questionsCount) + ". and action: " + action);

      if (action == "stopMechanic") {
        stopMechanic();
      }
      else if (action == "waitingResponseEnd") {
        p(F("Launch waitingResponseEnd"));
        action = "doNothing";
        simpleTimer.setTimeout(4 * 1000, waitingResponseEnd);
        return;

      }
      else if (action == "processChoice1") {
        processChoice('1');
      }
      else if (action == "lastQuestionWrong") {
        repeatQuestion();
      }
      else {
        p(F("Call NextQuestion"));
        nextQuestion();
      }
      p(F("Action set to empty"));
      action = "";
    }
  }
}

void repeatQuestion() {
  if (questionsCount == 9 || questionsCount == 10 || questionsCount == 11) {
    playAudio(playingFileName + "R", false);// DONE
  }
  else {
    playAudio(playingFileName, false); // DONE
  }
}

//////////////////////////////////////////////////// TIMERS

void waitingResponseEnd() {
  p(F("en waitingResponseEnd"));
  int isResponse = false;

  if (isResponse || isRepeated) {
    
    isRepeated = false;
    processChoice('1');
    action = "";
  }
  else {
    isRepeated = true;
    questionsCount--;
    action = "waitingResponseEnd";
    nextQuestion();
  }
}


/////////////////////////////////     SETUP / LOOP
void setup() {
  Serial.begin(9600);
  
  initVariables();
  lcd.begin(16, 2);  //LCD
  printLcd("RioPhoneHack #1", 0, "this", false);

  // Colgar State
  attachInterrupt(4, colgadoStateChanged, CHANGE);
  //isColgado = digitalRead(interruptPin);
  //previousIsColgado = isColgado;
  initPlayer();

  simpleTimer.setTimeout(randomRingInterval, randomRing);

  // BackLight
  pinMode(BL1, OUTPUT);
  // Buzzer
  pinMode(53, OUTPUT);
  digitalWrite(53, LOW);
  
  
  detectPresence = false;
  simpleTimer.setTimeout(waitAfterEnd, setDetectPresence);
  
  
}

// RESET FUNCTION. It actually resets the arduino "physically"
void(* resetFunc) (void) = 0; //declare reset function @ address 0

void loop() {
  simpleTimer.run();

  checkQuestions();
  checkPresence();

  readSerial();
  readKeypad();

  delay(delayLoop);
}
