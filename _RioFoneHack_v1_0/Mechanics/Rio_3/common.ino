#include "Arduino.h"

// Volume
uint8_t initialVolume = 10;
uint8_t volumeValueLeft = initialVolume; // LOUT for Handset!!
uint8_t volumeValueRight = initialVolume; // LOUT for Speaker!!


// Ringing & Presence
bool isRinging = false;
int presenceCount = 0;
const unsigned long waitAfterRing = 20000;
unsigned long waitAfterEnd = 30000;
int timerSetDetect;
int timerStopRinging;
int timerRandom;

unsigned long distance; // in cm
bool isPresence = false;

// Colgar / Descolgar
elapsedMillis initColgar = 2000;
elapsedMillis initDescolgar = 2000;


// Test
int goToQuestion = false;
String specialCode;



/////////////////////////// COLGAR / DESCOLGAR //////////////////////
void colgadoStateChanged() {

  isColgado = digitalRead(interruptPin);
  if (isColgado == previousIsColgado) {
    p(F("ColgadoState has not really changed. Do nothing  (preventing doble interrupt detected)"));
    return;
  }
  previousIsColgado = isColgado;

  if (isColgado) {
    colgar();
  }
  else {
    descolgar();
  }
}

void colgar() {

  if (initColgar > 1000) {
    p(F(" - Colgar"));
    stopMechanic();
    initColgar = 0;
  }
  else {
    p(F(" - - Colgar detected, but not done due to restriction time (last Colgar was detected in less than 1 second)"));
  }

}

void descolgar() {

  if (initDescolgar > 1000) {
    p(F(" - Descolgar"));
    startMechanic();
    initDescolgar = 0;
  }
  else {
    p(F(" - - Descolgar detected, but not done due to restriction time (last Descolgar was detected in less than 1 second)"));
  }
}






///////////////////////////// MECHANIC START  /  STOP
void startMechanic() {
  if (isPlayingMechanic) {
    p(F(" - - isPlayingMechanic is True, do not start Again"));
    return;
  }
  p(F(" - - Starting Mechanic"));

  // Clear Variables
  initVariables();
  setBacklight("on");
  setChannel('l');

  isPlayingMechanic = true;
  isRinging = false;
  detectPresence = false;

  simpleTimer.deleteTimer(timerRandom);

  startStats();

  /////////////////// This Mechanic Only
  keypadBlocked = false;
  ///////////////////

  nextQuestion();
}

void stopMechanic() {
  if (!isPlayingMechanic) {
    p(F("isPlayingMechanic is already False, do nothing"));
    return;
  }
  p(F("Mechanic End"));


  isPlayingMechanic = false;
  musicPlayer.softReset();
  musicPlayer.stopPlaying();

  endStats();

  // Start Timers for setDetect and RandomRing
  p(F("call setDetectPresence in secs: ")); p(String(waitAfterEnd));
  timerSetDetect = simpleTimer.setTimeout(waitAfterEnd, setDetectPresence);
  //p("timerSetDetect id: " + String(timerSetDetect));
  timerRandom = simpleTimer.setTimeout(randomRingInterval, randomRing);

  setBacklight("off");
  initPlayer();

  ////////// This Mechanic Only
  clearTimers();
  //////////

  p(F("freeMemory available after Mechanic ends: ")); p(String(freeMemory()));
}






//////////////////////// KEYPAD  ////////////////////////
void readSerial() {
  if (Serial.available() > 0)
  {
    char c = Serial.read();
    keyReceived(c);
  }
}

void readKeypad() {
  char key = keypad.getKey();
  if (key) {
    keyReceived(key);
  }
}



//- HELPERS
void p(String text) {
  if (testMode) {
    Serial.println(text);
  }

}



//////////////////////////////   KEYS    /////////////
unsigned int keyTimeLimit;
void keyReceived(char c) {

  p(F("Key Received: ")); p(String(c));

  keyTimeLimit = 1000;

  if (questionsCount == 2 || questionsCount == 8) {
    keyTimeLimit = 3000;
  }
  if (questionsCount == 5 || questionsCount == 7 || questionsCount == 9 || questionsCount == 10 || questionsCount == 11 ||
      (questionsCount == 12 && isWeekend)) {
    keyTimeLimit = 7000;
  }
  if (testMode) {
    keyTimeLimit = 10;
  }

  if (elapsedQuestion < keyTimeLimit) {
    p(F("Key received before keyTimeLimit, ignore"));
    return;
  }
  if (fullPlaying) {
    p(F("playing full, do not answer to keypad"));
    // Dev
    if (testMode && c == '0') {
      nextQuestion();
    }
    return;

  }

  if (c == 'd') {
    descolgar();
  }

  if (c == 'c') {
    colgar();
  }

  if (c == 'C') {
    if (!goToQuestion) {
      goToQuestion = true;
      printLcd("C", 0, "both", true);
      return;
    }
    else {
      questionsCount = specialCode.toInt() - 1;
      specialCode = "";
      goToQuestion = false;
      nextQuestion();
      return;
    }

  }
  if (c == '0' || c == '1' || c == '2' || c == '3' || c == '4' || c == '5' ||
      c == '6' || c == '7' || c == '8' || c == '9' || c == '#' ||
      (c == '*' && questionsCount == 14)) {
    processChoice(c);
    return;
  }


  if (c == '*') {
    beep();
    repeatQuestion();
  }


  // VOLUME
  if (c == 'A') {
    setVolumeUp();
  }
  if (c == 'B') {
    setVolumeDown();
  }
  if (c == 'D') {
    descolgar();
  }

  if (c == 'r') {
    startRinging(false);
  }

  if (c == 'i') {
    initGSM();
  }

  if (c == 't') {
    p(F("sendind SMS to Conducttr"));
    sendSMS("+447774235319", conducttrNumber);
  }
}

/////////////////////// THIS MECHANIC ONLY
//////////////////////// CLOCK
void initClock() {
  Wire.begin();
  RTC.begin();
  //RTC.adjust(DateTime(__DATE__, __TIME__));
  if (! RTC.isrunning()) {
    p(F("RTC is NOT running!"));
    // following line sets the RTC to the date & time this sketch was compiled
    //RTC.adjust(DateTime(__DATE__, __TIME__));
  }

  DateTime now = RTC.now();
  p(String(now.year()) + "/" + String(now.month()) + "/" + String(now.day()) + " " + String(now.hour()) + ":" + String(now.minute()) + ":" + String(now.second()));
}


////////////////////////
bool checkIsWeekend() {

  return false;
  /*
  if (false) {
    DateTime now = RTC.now();
    uint8_t dayWeek = now.dayOfWeek();

    uint8_t hours = now.hour();

    if ((dayWeek == 6 || dayWeek == 0) && (hours >= 10 && hours < 16)) {
      isWeekend = true;
    }
    else {
      isWeekend = false;
    }

    // Special Case   Weekend 4 and 5 July
    if ((now.day() == 4 || now.day() == 5) && now.month() == 7) {
      isWeekend = false;
    }

  }
  // ALWAYS False
  else {
    isWeekend = false;
  }
  // Set Some variables
  if (!isWeekend) {
    waitAfterEnd = 50000L; // Because of Conducttr
  }


  //p("WeekDay is " + String(dayWeek) + ". Hour is " + String(hours) + ".  isWeekend: " + String(isWeekend));
  p(String(now.year()) + "/" + String(now.month()) + "/" + String(now.day()) + " " + String(now.hour()) + ":" + String(now.minute()) + ":" + String(now.second()));
  return isWeekend;
  */
}

unsigned long startUnix;

void setStartUnix() {
  /*
  DateTime now = RTC.now();
  startUnix = now.unixtime();
  p(F("StartUnix con now "));
  Serial.println(startUnix);*/
  
  startUnix = 0;
}



