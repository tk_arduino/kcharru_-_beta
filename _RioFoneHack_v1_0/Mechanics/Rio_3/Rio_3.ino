#include <RioPhoneHack.h>
RioPhoneHack rio;

//- LCD   //////////////////////////////////////////////////////
#include <LiquidCrystal.h>
LiquidCrystal lcd(RS, EN, DB4, DB5, DB6, DB7);

// KEYPAD
#include <Keypad.h>
byte rowPins[ROWS] = {pinDown1, pinDown2, pinDown3, pinDown4}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {pinUp1, pinUp2, pinUp3, pinUp4}; //connect to the column pinouts of the keypad
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

#include <NewPing.h>
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

#include "SimpleTimer.h"
SimpleTimer simpleTimer;

#include <elapsedMillis.h>

// include SPI, MP3 and SD libraries
#include <SPI.h>
#include <Adafruit_VS1053.h>
#include <SD.h>

// PINS DEFINED in RioPhoneHack library
Adafruit_VS1053_FilePlayer musicPlayer = Adafruit_VS1053_FilePlayer(BREAKOUT_RESET, BREAKOUT_CS, BREAKOUT_DCS, DREQ, CARDCS);

#include <MemoryFree.h>

#include <Wire.h>
#include "RTClib.h"

RTC_DS1307 RTC;

/////////////////////////// Config Test Values
bool testMode = true;
///////////////////////////

//////////////////////////// GLOBALS
// General Variables


int interruptPin = 19;
bool isColgado = true; 
bool previousIsColgado = true;
bool detectPresence = true;

String playingFileName;
String answerAudio;
String action;
String userInput;

bool isPlayingMechanic;
elapsedMillis elapsedQuestion;
unsigned long elapsedLimit = 2000;
bool fullPlaying;
const unsigned long randomRingInterval = 60000;
bool keypadBlocked;


//String rioHack1Number = "07548233700";
String conducttrNumber = "07903579560";


// Rio 3 Variables
int rioNumber = 3;
int delayLoop = 10;
String userPhoneNumber;

const int numberOfQuestions = 18;
int choices[numberOfQuestions];

int questionsCount;
const int maxChoices = 3 - 1;
const String folder = String(rioNumber) + "/RH" + String(rioNumber) + "_";

int isWeekend;

int waitingForConducttr1EndTimer;
int waitingForConducttr_2_EndTimer;
int waitingForBRIDGETimer;


// Init global variables
void initVariables(){
  
  playingFileName = "";
  answerAudio = "";
  action = "";
  userInput = "";
  
  ////////This Mechanic Only
  userPhoneNumber = "";
  keypadBlocked = false;
  ////////
  
  isPlayingMechanic = false;
  elapsedQuestion = 0;
  fullPlaying = false;
  
  
  
  // OJO
  isWeekend = false;
  questionsCount = 0;
  //choices[numberOfQuestions];
  
  // clearTimer
  
}

String questions[numberOfQuestions + 1][3] = {
  {"nothingHere", "nothingAnswer", "0"},
  // 1
  {"notAnswer", "notAllowed", "notAllowed"},
  // 2
  {"notAnswer", "notAnswer", "notAnswer"},
  // 3.   1 goes to 5th question
  {"003a.mp3", "notAnswer", "notAllowed"},
  // 4 (Amount Type) NOT Answers
  {"amount", "notAnswer", "notAnswer"},
  // 5
  {"notAnswer", "notAnswer", "notAnswer"},
  // 6
  {"notAnswer", "notAnswer", "notAllowed"},
  // 7 (Amount Type) with 4 answers
  {"amount", "notAnswer", "notAnswer"},
  // 8
  {"notAnswer", "notAnswer", "notAnswer"},
  // 9  3 Option, 2 Answers
  {"009a.mp3", "009b.mp3", "009a.mp3"},
  // 10  3 Option, 2 Answers
  {"010a.mp3", "010a.mp3", "010b.mp3"},
  // 11.
  {"011a.mp3", "011a.mp3", "011a.mp3"},
  // 12.  Over after pressing 1 ( = NO )
  {"012a.mp3", "notAnswer", "notAllowed"},
  // 13 Phone Number!
  {"amount", "notAnswer", "notAnswer"},
  // 14 Phone Number! Confirm the number on display
  // 1 goes to 15.  * Goes BACK to 13
  {"notAnswer", "notAllowed", "notAllowed"},
  // 15   WAITING for Conducttr  to call the player (60 secs)
  {"notAllowed", "notAllowed", "notAllowed"},
  // 16   WAITING for THE PLAYER  to TEXT (2 minutes)
  {"notAnswer", "notAllowed", "notAllowed"},
  // 17   WAITING for Conducttr  to call the player OWN PHONE (60 secs)
  {"notAllowed", "notAllowed", "notAllowed"},
  // 18  FINAL MESSAGE
  {"notAllowed", "notAllowed", "notAllowed"}

};


/////////////////////////  MECHANIC /////////////////////////
void nextQuestion() {

  questionsCount++;

  // Must be check on firstQuestion
  if(questionsCount == 1){
     isWeekend = checkIsWeekend(); 
  }
  
  addToStats(" Q" + String(questionsCount));

  userInput = "";
  lcd.begin(16, 2);  //LCD
  printLcd("", 0, "both", false);

  printQuestion(' ');

  // Tell Sebs
  if (isWeekend) {
    if (questionsCount == 5 || questionsCount == 10 || questionsCount == 12) {
      //sendNotification();
    }
  }


  if (isWeekend) {
    if (questionsCount == 12) {
      
      action = "stopMechanic";
      playingFileName = "3/RH3_012w.mp3";
      playAudio(playingFileName, true);
      return;
    }
  }

  // Special Cases 14 - 7
  if (questionsCount == 14) {
    //
  }
  if (questionsCount == 15) {
    playingFileName = rio.getQuestionFileName(folder, questionsCount);
    playAudio(playingFileName, false);

    unsigned long waitTime1 = 60000;
    waitingForConducttr1EndTimer = simpleTimer.setTimeout(waitTime1, waitingForConducttr1End);

    // Remove the '0' and add +44
    String formattedUserNumber = "+44" + userPhoneNumber.substring(1);
    sendSMS(formattedUserNumber, conducttrNumber);

    keypadBlocked = true;
    
    return; // So it does not play twice!
  }
  if (questionsCount == 16) {
    // Nothing Here
    playingFileName = rio.getQuestionFileName(folder, questionsCount);
    playAudio(playingFileName, false);
    action = "doNothing";
    
    return;
  }
  if (questionsCount == 17) {
    unsigned long waitTime2 = 60000;
    waitingForConducttr_2_EndTimer = simpleTimer.setTimeout(waitTime2, waitingForConducttr_2_End);
    keypadBlocked = true;
  }

  playingFileName = rio.getQuestionFileName(folder, questionsCount);
  playAudio(playingFileName, false);

  // Check END
  if ((questionsCount > numberOfQuestions) || (isWeekend && questionsCount > 12)) {
    
    stopMechanic();
    return;
  }

}

// Options
void processChoice(char choice) {

  p("processChoice " + String(choice) + " of Q " + String(questionsCount));
  
  addToStats(" C" + String(choice));
  
  // Get choice
  int convertedChoice = choice - '0';
  choices[questionsCount] = convertedChoice;
  convertedChoice--;

  if (questionsCount == 14 && choice == '*') {
    beep();
    questionsCount = 13 - 1;
    nextQuestion();
    return;
  }
  if (questionsCount == 16 && choice == '1') {
    simpleTimer.deleteTimer(waitingForBRIDGETimer);
    // Goes to 17
  }


  // Get Type
  String type = questions[questionsCount][0];

  if (choice == '#' && type != "amount") {
    return;
  }

  // It's Amount. Wait for #
  if (type == "amount") {
    beep();
    if (choice != '#') {
      userInput = userInput + String(choice);
      if (questionsCount == 13) {
        printLcd("Your number", 0, "both", false);
        printLcd(String(userInput), 1, "none", false);
      }
      else {
        String sufix = rio.getSufix(questionsCount, userInput.toInt());
        printLcd(String(userInput) + sufix, 0, "both", false);
      }
      return;
    }
    // Introduction of answer End
    else {
      if (questionsCount == 7) {
        int amount = userInput.toInt();
        answerAudio = rio.getRio3Answer7(amount);
      }
      if (questionsCount == 13) {
        userPhoneNumber = userInput;
      }
      userInput = "";
    }
  }
  // NOT an amount
  else {
    if (convertedChoice > maxChoices) {
      p(F("Not Allowed, maxChoices"));
      return;
    }

    answerAudio = questions[questionsCount][convertedChoice];

    if (answerAudio == "notAllowed") {
      p(F("Key NOT ALLOWED! Try again"));
      return;
    }
  }
  printQuestion(choice);
  beep();
  /////   WE ARE IN ProcessChoice //////////


  // Special cases
  if (questionsCount == 3 && choice == '1') {
    questionsCount = 5 - 1; // It goes to 5   (nextQuestions will increment 1, so we must substract 1)
  }
  
  playingFileName = folder + answerAudio;

  if (answerAudio != "notAnswer") {
    playAudio(playingFileName, true);
  }
  else {
    p(F("Player Key received. Not Answer, goes to the next Question"));
    nextQuestion();
  }


  if (!isWeekend && questionsCount == 12 && choice == '1') { // Not want to take a walk => END
    action = "stopMechanic";
    return;
  }
}


void checkQuestions() {

  if (!isPlayingMechanic) {
    return;
  }
  // Just check if Full
  if (fullPlaying) {
    if (action == "doNothing") {
      return;
    }
    
    if ((elapsedQuestion > elapsedLimit) && (!musicPlayer.playingMusic)) {
      elapsedQuestion = 0;
      p(F(" -- Audio playing end for question ")); p(String(questionsCount) + ". Action is: " + action);
      
      if (action == "stopMechanic") {
        stopMechanic();
      }
      else {
        p(F("- Call NextQuestion"));
        nextQuestion();
      }
      action = "";
    }
  }
}

void repeatQuestion() {
  userInput = "";
  if(questionsCount == 13){
        printQuestion(' ');
        return;
    }
    else{
      //printLcd("", 0, "both", false);
    }
  if (questionsCount == 9 || questionsCount == 10 || questionsCount == 11) {
    if (questionsCount == 9) {
      playAudio("3/RH3_009R.mp3", false);
    }
    if (questionsCount == 10) {
      playAudio("3/RH3_010R.mp3", false);
    }
    if (questionsCount == 11) {
      playAudio("3/RH3_011R.mp3", false);
    }

  }
  else {
    playAudio(playingFileName, false);
  }
}

//////////// Timers Functions
void waitingForConducttr1End() {

  keypadBlocked = false;
  p(F("waitingForConducttr1End End, go to nextQuestion. And setTime for 16 - BRIDGE"));
  nextQuestion();
  unsigned long waitTimeBridge = 60000;
  waitingForBRIDGETimer = simpleTimer.setTimeout(waitTimeBridge, waitingForBRIDGE);
}
void waitingForBRIDGE() {
  p(F("waitingForBRIDGE executing. Goes to 18"));
  questionsCount = 18 - 1;
  nextQuestion();
}
void waitingForConducttr_2_End() {
  p(F("waitingForConducttr_2 END. Goes to 18"));
  keypadBlocked = false;
  nextQuestion(); // Goes to 18 = END
}

void clearTimers() {
  
  simpleTimer.deleteTimer(waitingForConducttr1EndTimer);
  simpleTimer.deleteTimer(waitingForConducttr_2_EndTimer);
  simpleTimer.deleteTimer(waitingForBRIDGETimer);
  
}



void setup() {
  Serial.begin(9600);

  initVariables();

  lcd.begin(16, 2);  //LCD
  printLcd("RioPhoneHack#3", 0, "this", false);

  // Hang Up
  attachInterrupt(4, colgadoStateChanged, CHANGE);
  
  // BackLight
  pinMode(BL1, OUTPUT);
  // Buzzer
  pinMode(53, OUTPUT);
  initPlayer();

  initClock();
  
  simpleTimer.setInterval(randomRingInterval, randomRing);
  
  if(!testMode){
    initGSM();
  }
  else{
   p(F(" > > GSM not initialized, we are in TestMode")); 
  }
  
  setStartUnix();
  
}

void loop() {
  simpleTimer.run();
  checkQuestions();
  
  checkPresence();

  readSerial();
  readKeypad();
  
  readGsmSerial();

  delay(delayLoop);
}
