#include "Arduino.h"

// LCD
String text1;
String text2;
int timerShowMessage;
int timerShowMessage2;
unsigned long showMessageDelay;

//////////////////////// LCD   ////////////////////////
void printLcd(String text, int line, String mustClear, bool justTest) {
  if (justTest && !testMode) {
    return;
  }

  text = " " + text; // Add a space at the beginning
  if (mustClear == "both") {
    lcd.clear();
  }

  lcd.setCursor(0, line);

  if (mustClear == "this") {
    lcd.print("                   ");
    lcd.setCursor(0, line);
  }

  lcd.print(text);
}


void showMessage() {
  printLcd(text1, 0, "both", false);
  printLcd(text2, 1, "none", false);
}

void showBridgeMessage() {
  text1 = "Text BRIDGE to";
  text2 = "07903579560";
  showMessage();
}

void printQuestion(char choice) {
  
  simpleTimer.deleteTimer(timerShowMessage);
  simpleTimer.deleteTimer(timerShowMessage2);
  
  text1 = "";
  text2 = "";
  
  // CLEAR 
  showMessage();
  
  String type = "";
  String brazilianName = "";
  int question = questionsCount;

  showMessageDelay = 0;



  // It IS an ANSWER!!
  if (choice != ' ') {
    if(question == 11){
       showMessageDelay = 3000; 
    }

    if (question == 9 && (choice == '1' || choice == '3')) {
      text1 = "Not correct";
    }
    if (question == 9 && choice == '2') {
      text1 = "Correct";
    }
    if (question == 10 && (choice == '1' || choice == '2')) {
      text1 = "Not correct";
    }
    if (question == 10 && choice == '3') {
      text1 = "Correct";
    }
    if (question == 11) {
      text1 = "Connected but";
      text2 = "unknowable";
    }
    if (question == 12) {
      text1 = "How close is";
      text2 = "too close ?";
    }
  }
  else {
    
    if(question == 2 || question == 8){
       showMessageDelay = 3000; 
    }
    if(question == 5 || question == 7 || question == 9 || question == 10 || question == 11 || 
    (question == 12 && isWeekend)){
       showMessageDelay = 7000; 
    }
    
    switch (question) {
      case 1:
        text1 = "Scientific";
        text2 = "Experiment";
        break;

      case 2:
        text1 = "1=Good  2=Fair";
        text2 = "3=Poor";
        break;

      case 3:
        text1 = "Press 1 for No";
        text2 = "Press 2 for Yes";
        break;

      case 4:
      case 7:
        text1 = "Enter number &";
        text2 = "press # key";
        break;

      case 5:
        text1 = "1=Many  2=Few";
        text2 = "3=None";
        break;

      case 6:
        text1 = "Press 1 for Yes";
        text2 = "Press 2 for No";

        break;

      case 8:
        text1 = "1=Open  2=Dreamy";
        text2 = "3=Calm";
        break;


      case 9:
        text1 = "1=35";
        text2 = "2=42  3=56";
        break;

      case 10:
        text1 = "1=6tri  2=4oct";
        text2 = "3=1sept";
        break;

      case 11:
        text1 = "1=While ago 2=";
        text2 = "Long ago 3=Never";
        break;

      case 12:
        if (isWeekend) {
          text1 = "How close is";
          text2 = "too close ?";
        }
        else {
          text1 = "Walk with me ?";
          text2 = "1=No  2=Yes";
        }

        break;
      case 13:
        text1 = "Enter phone number";
        text2 = "then #";
        break;

      case 14:
        text1 = userPhoneNumber;
        text2 = "Check number";
        break;

      case 15:
        text1 = "I am trying to";
        text2 = "contact you";
        timerShowMessage2 = simpleTimer.setTimeout(30000, showBridgeMessage);
        break;

      case 16:
        text1 = "BRIDGE to 07903";
        text2 = "579560 &press 1";
        break;

      case 17:
        text1 = "I am trying to";
        text2 = "get in touch";
        break;

      case 18:
        text1 = "Sorry";
        break;

      default:

        break;

    }
  }
  

  timerShowMessage = simpleTimer.setTimeout(showMessageDelay, showMessage);

}

void setBacklight(String state) {
  if (state == "on") {
    digitalWrite(BL1, HIGH);
  }
  else {
    digitalWrite(BL1, LOW);
    printLcd("", 0, "both", false);
  }
}
