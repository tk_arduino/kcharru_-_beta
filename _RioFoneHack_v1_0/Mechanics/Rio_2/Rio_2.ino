#include <RioPhoneHack.h>
RioPhoneHack rio;

#include <NewPing.h>
NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

//- LCD   //////////////////////////////////////////////////////
#include <LiquidCrystal.h>
LiquidCrystal lcd(RS, EN, DB4, DB5, DB6, DB7);

// KEYPAD
#include <Keypad.h>
byte rowPins[ROWS] = {pinDown1, pinDown2, pinDown3, pinDown4}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {pinUp1, pinUp2, pinUp3, pinUp4}; //connect to the column pinouts of the keypad
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );


#include "SimpleTimer.h"
SimpleTimer simpleTimer;

#include <elapsedMillis.h>


// AUDIO PLAYER
#include <SPI.h>
#include <Adafruit_VS1053.h>
#include <SD.h>

// PINS DEFINED in RioPhoneHack library
Adafruit_VS1053_FilePlayer musicPlayer = Adafruit_VS1053_FilePlayer(BREAKOUT_RESET, BREAKOUT_CS, BREAKOUT_DCS, DREQ, CARDCS);


#include <MemoryFree.h>

/////////////////////////// Config Test Values
bool testMode = true;


//////////////////////////// GLOBALS
// General Variables
int interruptPin = 19;
bool isColgado = true; 
bool previousIsColgado = true;
bool detectPresence = true;

String playingFileName;
String action;

bool isPlayingMechanic;
elapsedMillis elapsedQuestion;
unsigned long elapsedLimit = 2000;
bool fullPlaying;
const unsigned long randomRingInterval = 60000L;

// Rio 3 Variables
const int rioNumber = 2;
const int numberOfQuestions = 4;
int choices[numberOfQuestions];
int questionsCount;
const String folder = String(rioNumber) + "/RH" + String(rioNumber) + "_";
int delayLoop = 10;

bool isFingerDetected = false;

int waitForSensor;
int waitForSecondQuestion;


String lcdMessage;
int lcdTimer;
long sensorTime =  50000L;

long questionTime = 132000L;

// Init global variables
void initVariables(){
  
  playingFileName = "";
  action = "";
  lcdMessage = "";

  
  isPlayingMechanic = false;
  elapsedQuestion = 0;
  fullPlaying = false;
  
  questionsCount = 0;
  //choices[numberOfQuestions];
  
}

////////////////////////////////////////////////   MECHANIC   ////////////////
void nextQuestion() {
  if (!isPlayingMechanic) {
    return;
  }
  elapsedQuestion = 1;
  lcd.begin(16, 2);  //LCD
  questionsCount++;

  addToStats(" Q" + String(questionsCount));

  playingFileName = rio.getQuestionFileName(folder, questionsCount);

  if (questionsCount == 1) {
    playAudio(playingFileName, false);
    waitForSensor = simpleTimer.setTimeout(sensorTime, startSensor);

    elapsedQuestion = 1;
    lcdMessage = "welcome";
    printSpecial();
    lcdMessage = "breathe";
    lcdTimer = simpleTimer.setTimeout(20000, printSpecial);

    return;
  }
  if (questionsCount == 2) {
    stopSensor();

    if (!isFingerDetected) {
      addToStats(" Finger not Detected, go to Q3 ");
      questionsCount = 3;
    }
    lcdMessage = "heartbeat";
      printSpecial();
    
  }
  if (questionsCount == 3 && isFingerDetected) {
    // If isFingerDetected, audio 2 was already played, goes to 4
    questionsCount = 4;
  }
  playingFileName = rio.getQuestionFileName(folder, questionsCount);
  // FULL Play!
  playAudio(playingFileName, false);

  // Check END
  if (questionsCount > 4) {

    lcdMessage = "end";
    printSpecial();
    stopMechanic();
    return;
  }
  //nextQuestion();

}


void repeatQuestion() {
  playAudio(playingFileName, false);
}


int globalBeats;


void checkQuestion1() {

  if ((isPlayingMechanic) && (elapsedQuestion > 10000) && (!musicPlayer.playingMusic)) {

    elapsedQuestion = 0; // Just in case
    nextQuestion();
    p(F("End Question ")); p(String(questionsCount)); p(F(", go to nextQuestion"));
  }
}

void clearTimers() {
  simpleTimer.deleteTimer(waitForSensor);
  simpleTimer.deleteTimer(lcdTimer);
}




void setup() {

  Serial.begin(9600);
  lcd.begin(16, 2);  //LCD
  printLcd("RioPhoneHack #2", 0, "this", false);

  // Hang Up
  attachInterrupt(4, colgadoStateChanged, CHANGE);

  initPlayer();


  simpleTimer.setTimeout(randomRingInterval, randomRing);
  //simpleTimer.setInterval(resetInterval, reset);
  // Buzzer
  pinMode(BL1, OUTPUT);
  // Buzzer
  pinMode(53, OUTPUT);
  digitalWrite(53, LOW);

  setupSensor();
}

void loop() {
  checkQuestion1();

  simpleTimer.run();
  checkSensor();

  checkPresence();

  readSerial();
  readKeypad();

  delay(delayLoop);

}
