#include "Arduino.h"


/////// ///////  PRESENCE 

void setDetectPresence() {
  if (!isPlayingMechanic) {
    p(F("setDetectPresence = true"));
    detectPresence = true;
  }
  else {
    p(F("setDetectPresence NOT set to true because isPlayingMechanic!!"));
  }
}

void checkPresence() {

  if (detectPresence && isColgado) {

    unsigned int uS = sonar.ping(); // Send ping, get ping time in microseconds (uS).
    unsigned int cm = uS / US_ROUNDTRIP_CM;

    isPresence = (cm != 0 && cm > 12);
    p(String(cm) + " cm. isPresence " + String(isPresence));

    if (isPresence) {
      if (presenceCount > 1) {
        p(F("presenceDetected -> startRinging"));
        presenceCount = 0;
        startRinging(false);
      }
      else {
        presenceCount++;
      }
    }
    else {
      presenceCount = 0;
    }
    delay(100);
  }

}

/////////////////////// RING ///////////////////
void startRinging(bool isRandom) {
  
  // Do not Ring if: 
  if (isRinging || !isColgado || isPlayingMechanic || !detectPresence) {
    p(F("Do NOT ring"));
    return;
  }

  setChannel("both");
  if (isRandom && testMode) {
    playAudio("ringram.mp3", false);
  }
  else {
    playAudio("ring.mp3", false);
  }

  isRinging = true;
  timerStopRinging = simpleTimer.setTimeout(20000, stopRinging);
}

void stopRinging() {
  if (!isRinging) {
    return;
  }
  p(F("Stop Ringing"));
  isRinging = false;
  musicPlayer.stopPlaying();
}


void randomRing() {

  timerRandom = simpleTimer.setTimeout(randomRingInterval, randomRing);
  p(F("randomRing"));
  if (isPlayingMechanic || isRinging) {
    p(F("no llama a ring porque tiene isPlayingMechanic "));  p(String(isPlayingMechanic) + " y  isRinging " + String(isRinging));
    return;
  }
  bool doit = rio.getRandomRing();

  p(F("Resultado del random es ")); p(String(doit) );
  if (doit) {
    startRinging(true);
  }
}
