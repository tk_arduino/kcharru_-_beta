#include "Arduino.h"

// Volume
uint8_t initialVolume = 0;
uint8_t volumeValueLeft = initialVolume; // LOUT for Handset!!
uint8_t volumeValueRight = initialVolume; // LOUT for Speaker!!


// Ringing & Presence
bool isRinging;
int presenceCount = 0;
const unsigned long waitAfterRing = 20000;
unsigned long waitAfterEnd = 30000;
int timerSetDetect;
int timerStopRinging;
int timerRandom;

unsigned long distance; // in cm
bool isPresence = false;

// Colgar / Descolgar
elapsedMillis initColgar = 2000;
elapsedMillis initDescolgar = 2000;


// Test
int goToQuestion = false;
String specialCode;



/////////////////////////// COLGAR / DESCOLGAR //////////////////////
void colgadoStateChanged() {

  isColgado = digitalRead(interruptPin);
  if (isColgado == previousIsColgado) {
    p(F("El estado de colgado no ha cambiado, return"));
    return;
  }
  previousIsColgado = isColgado;

  if (isColgado) {
    colgar();
  }
  else {
    descolgar();
  }
}

void colgar() {
  if (initColgar > 1000) {
    p(F(" HangUP "));
    stopMechanic();
    initColgar = 0;
  }
  else {
    p(F(" Do NOT HangUP"));
  }

}

void descolgar() {
  if (initDescolgar > 1000) {
    p(F(" - PickUP"));
    startMechanic();
    initDescolgar = 0;
  }
  else {
    p(F(" - Do NOT PickUp"));
  }
}

///////////////////////////// MECHANIC START  /  STOP
void startMechanic() {
  p(F("Starting Mechanic"));
  if (isPlayingMechanic) {
    p(F("ya esta isPlayingMechanic, no hace nada"));
    return;
  }
  // Clear Variables
  initVariables();
  setBacklight("on");
  setChannel("left");

  isPlayingMechanic = true;
  isRinging = false;
  detectPresence = false;

  simpleTimer.deleteTimer(timerStopRinging);

  startStats();

  nextQuestion();
}

void stopMechanic() {

  if (!isPlayingMechanic) {
    p(F("NO esta isPlayingMechanic, no hace nada"));
    return;
  }
  p(F("Mechanic End"));

  isPlayingMechanic = false;
  musicPlayer.softReset();
  musicPlayer.stopPlaying();

  endStats();

  // Start Timers for setDetect and RandomRing
  //simpleTimer.deleteTimer(timerSetDetect);
  //delay(1000);
  p(F("call setDetectPresence in secs: ")); p(String(waitAfterEnd));
  timerSetDetect = simpleTimer.setTimeout(waitAfterEnd, setDetectPresence);
  timerRandom = simpleTimer.setTimeout(randomRingInterval, randomRing);

  setBacklight("off");
  initPlayer();

  clearTimers();
  stopSensor();
  
  
  p(F("freeMemory()="));
  p(String(freeMemory()));
}



//////////////////////// KEYPAD  ////////////////////////
void readSerial() {
  if (Serial.available() > 0)
  {
    char c = Serial.read();
    keyReceived(c);
  }
}

void readKeypad() {

  char key = keypad.getKey();
  if (key) {
    keyReceived(key);
  }
}



//- HELPERS
void p(String text) {
  if (testMode) {
    Serial.println(text);
  }
}


void keyReceived(char c) {
  if(c == 'd'){
     descolgar(); 
  }
  if(c == 'c'){
     colgar(); 
  }
}

