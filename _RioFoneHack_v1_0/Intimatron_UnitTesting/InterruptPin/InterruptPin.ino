int pin = 13;
volatile int state = LOW;

int inPin = 3;
int val = 0;
void setup()
{
  Serial.begin (9600);
  pinMode(pin, OUTPUT);
  // FALLING, RISING or CHANGE
  attachInterrupt(1, blink, CHANGE);
  
  
}

void loop()
{
  digitalWrite(pin, state);
}

void blink()
{
  Serial.println("Changed! ");
  state = !state;
  
  Serial.println("ledPin state " + String(state));
  val = digitalRead(inPin);
  
  Serial.println("pin interruption, estado: " + String(val));
  delay(1000);
}
