#include <RioPhoneHack.h>
RioPhoneHack rio;

//- LCD   //////////////////////////////////////////////////////
#include <LiquidCrystal.h>
LiquidCrystal lcd(RS, EN, DB4, DB5, DB6, DB7);

// KEYPAD
#include <Keypad.h>
byte rowPins[ROWS] = {pinDown1, pinDown2, pinDown3, pinDown4}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {pinUp1, pinUp2, pinUp3, pinUp4}; //connect to the column pinouts of the keypad
Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );


// include SPI, MP3 and SD libraries
#include <SPI.h>
#include <Adafruit_VS1053.h>
#include <SD.h>

// PINS DEFINED in RioPhoneHack library
Adafruit_VS1053_FilePlayer musicPlayer = Adafruit_VS1053_FilePlayer(BREAKOUT_RESET, BREAKOUT_CS, BREAKOUT_DCS, DREQ, CARDCS);
// create shield-example object!
//Adafruit_VS1053_FilePlayer(SHIELD_RESET, SHIELD_CS, SHIELD_DCS, DREQ, CARDCS);



// General Variables
String playingFileName;
int delayLoop = 10;


void startRinging() {

  playingFileName = "ring.mp3";
  playAudio(playingFileName, true);

}

////// PLAYER
void initPlayer() {
  if (! musicPlayer.begin()) { // initialise the music player
    Serial.println(F("Couldn't find VS1053, do you have the right pins defined?"));
    while (1);
  }
  Serial.println(F("VS1053 found"));

  SD.begin(CARDCS);    // initialise the SD card
  // Set volume for left, right channels. lower numbers == louder volume!
  musicPlayer.setVolume(10, 10);

  // If DREQ is on an interrupt pin (on uno, #2 or #3) we can do background
  // audio playing
  musicPlayer.useInterrupt(VS1053_FILEPLAYER_PIN_INT);  // DREQ int
}

void playAudio(String filename, bool full) {

  p("Start playing filename " + filename);
  musicPlayer.stopPlaying();

  char charArray[filename.length()];
  filename.toCharArray(charArray, filename.length() + 1);

  if (full) {
    musicPlayer.playFullFile(charArray);
  }
  else {
    musicPlayer.startPlayingFile(charArray);
  }

  p("Playing filename " + filename);
  delay(200);

}

//- HELPERS
void p(String text) {
  Serial.println(text);
}

void readSerial() {
  if (Serial.available() > 0)
  {
    char c = Serial.read();
    Serial.println(String(c));
  }
}



void setup() {
  Serial.begin(9600);
   initPlayer();
   startRinging();
   startRinging();
   startRinging();
   startRinging();
    
}

void beep(int pin, int duration){
  //analogWrite(13, 100);  
  Serial.println("Beep");
  tone(pin, 440, duration);
  //musicPlayer.sineTest(0x44, 500);
}

int beepDuration = 10 * 1000;
void loop() {

   p("hago beep");
  beep(13, beepDuration);
  delay(10 * 1000);
  
  //readSerial();
  
  //delay(15*1000);
  //delay(1);

}
