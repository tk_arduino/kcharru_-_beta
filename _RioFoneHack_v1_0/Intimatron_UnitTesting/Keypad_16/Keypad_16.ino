/* @file HelloKeypad.pde
|| @version 1.0
|| @author Alexander Brevig
|| @contact alexanderbrevig@gmail.com
||
|| @description
|| | Demonstrates the simplest use of the matrix Keypad library.
|| #
*/
#include <Keypad.h>

const byte ROWS = 4; //four rows
const byte COLS = 4; //three columns

// 42 - 49
const int pinUp1   = 43; 
const int pinDown1 = 42;
const int pinUp2   = 45;
const int pinDown2 = 44;
const int pinUp3   = 47;
const int pinDown3 = 46;
const int pinUp4   = 49;
const int pinDown4 = 48;


char keys[ROWS][COLS] = {
  {'A', '1', '2', '3'},
  {'B', '4', '5', '6'},
  {'C', '7', '8', '9'},
  {'D', '*', '0', '#'}
};

byte rowPins[ROWS] = {pinDown1, pinDown2, pinDown3, pinDown4}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {pinUp1, pinUp2, pinUp3, pinUp4}; //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

void setup(){
  Serial.begin(9600);
}
  
void loop(){
  char key = keypad.getKey();
  
  if (key){
    Serial.println(key);
  }
}
