///** INCLUDES **///

#include "Adafruit_WS2801.h" // LIB LEDS
#include "SPI.h" // LIB SPI
#include <ESP8266WiFi.h>
#include <WifiLocation.h>
#include <CapacitiveSensor.h>


///** INIT LEDS **///
uint8_t dataPin  = 4;    
uint8_t clockPin = 5;    
Adafruit_WS2801 strip = Adafruit_WS2801(27, dataPin, clockPin);


///** INIT WIFI+API **///
const char* googleApiKey = "AIzaSyAp2vRHnQol8RqyocdOYyTOxGEm3M2ucpo";
const char* ssid = "ParaisoLC";
const char* passwd = "nomepreguntes";
//const char* ssid = "FWiFi";
//const char* passwd = "riodejaneiro";
WifiLocation location(googleApiKey);

///** INIT BLUEMAP **///
float latitude;
float longitude;
boolean inBlueMap = false;

///** INIT KAPPAPIN **///
const int ledPin = LED_BUILTIN;
boolean inTermitenteRight = false;
boolean inTermitenteLeft = false;

int kappaRightIN = 14; 
//int kappaRightOUT = 16;  
int kappaLeftIN = 12; 
//int kappaLeftOUT = 12;  
//CapacitiveSensor   kappaRight = CapacitiveSensor(kappaRightIN, kappaRightOUT); // 1M resistor in between pins
int kappaRightStatus = LOW;  
int valL = 0;
//int rR;           
//int pR = LOW;    
//long tiempoR = 0;       
//long debounceR = 200;
//CapacitiveSensor   kappaLeft = CapacitiveSensor(kappaLeftIN, kappaLeftOUT); // 1M resistor in between pins
int kappaLeftStatus = LOW;  
int valR = 0;
//int kappaLStatus = HIGH;  
//int rL;           
//int pL = LOW;    
//long tiempoL = 0;       
//long debounceL = 200;


///** INIT FRAKE **///
int frakeButton = A0;
boolean frakeOn = false;
boolean frakeRestart = false;


///** INIT TIMER **///
unsigned long previousMillis  = 0;
unsigned long currentMillis   = 0;
int interval = 60000;


////////////////////////////////////////// SETUP ///////////////////////////////////////////
void setup() {

    Serial.begin(115200);
    
    ///** SETUP LEDS **///
    strip.begin();
    strip.show();
    
    ///** SETUP KAPPAPIN **///
    pinMode(ledPin, OUTPUT);
    //pinMode(kappaRightIN, INPUT);
    //pinMode(kappaLeftIN, INPUT);
    pinMode(kappaRightIN, INPUT_PULLUP); 
    pinMode(kappaLeftIN, INPUT_PULLUP); 
  

    ///** SETUP FRAKE **///
    pinMode(frakeButton, INPUT);
  
    ///** SETUP WIFI **///
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, passwd);
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print("Attempting to connect to WPA SSID: ");
        Serial.println(ssid);
        Serial.print("Status = ");
        Serial.println(WiFi.status());
        rainbowCycle(1);
        delay(1000);
    }
    rainbow(26);
    rainbow(26);
    colorWipeReverse(Color(0, 0, 0), 5);
    verdeWipe(Color(0, 255, 0), 25);

}


///////////////////////////////////////////// LOOP ///////////////////////////////////////////
void loop() {

    ///** TIMER **///
    currentMillis = millis();
   // Serial.println("MILLIS = " + currentMillis);
                
    ///** KAPA RIGHT **///
  /*  valR = digitalRead(kappaRightIN);
    //Serial.println("kappaRightStatus = " + kappaRightStatus);
    if (valR) {

    */
    if (digitalRead(kappaRightIN) == false) {
          inTermitenteRight = true;
          } else {
              inTermitenteRight = false;    
          }
    
     /*   if (kappaRightStatus == HIGH) {
            kappaRightStatus = LOW;
        } else {
            kappaRightStatus = HIGH;
        }   */

        /*
        inTermitenteRight = true;
    } else {
        inTermitenteRight = false;    
    }
    */
    //digitalWrite(LED_BUILTIN, kappaRightStatus);


    ///** KAPA LEFT **///
    if (digitalRead(kappaLeftIN) == false) {
          inTermitenteLeft = true;
          } else {
              inTermitenteLeft = false;    
          } 
    //digitalWrite(LED_BUILTIN, kappaLeftStatus);
    


    ///** FRAKE **///
    int frakeStatus = analogRead(frakeButton);
    //delay(5);
    if (frakeStatus == 1024) {
        frakeOn = false;          
    } else {
        frakeOn = true;
    }

    while (frakeOn) {
        rojoFull(Color(255, 0, 0), 5);
       
        //Serial.println("frakeON");
        frakeStatus = analogRead(frakeButton);

          ///** KAPA RIGHT **///
        /*  valR = digitalRead(kappaRightIN);
          //Serial.println("kappaRightStatus = " + kappaRightStatus);
          if (valR) {
          */

              if (digitalRead(kappaRightIN) == false) {
                inTermitenteRight = true;
                } else {
                    inTermitenteRight = false;    
                }

    
             /* if (kappaRightStatus == HIGH) {
                  kappaRightStatus = LOW;
              } else {
                  kappaRightStatus = HIGH;
              }   */
       /*       inTermitenteRight = true;
          } else {
              inTermitenteRight = false;    
          }
        */
        ///** KAPA LEFT **///     
       if (digitalRead(kappaLeftIN) == false) {
            inTermitenteLeft = true;
        } else {
            inTermitenteLeft = false;    
        }        
    
        if (inTermitenteRight) {
            naranjaRight(Color(155, 125, 0), 0);
            delay(250);
    
        } else if (inTermitenteLeft) {
            naranjaLeft(Color(155, 125, 0), 0);
            delay(250);
            
        } else {
            //delay(2000);
        }
        //rojoFull(Color(255, 0, 0), 5);
       
        
        if (frakeStatus == 1024) {
            frakeOn = false;
            frakeRestart = true;
            Serial.println("frakeOFF");
        }


    }
     if (frakeRestart) {
            if (inBlueMap) {
                colorWipeReverse(Color(0, 0, 0), 0);
                azulWipe(Color(0, 0, 255), 0);  
            } else {
                colorWipeReverse(Color(0, 0, 0), 0);
                verdeWipe(Color(0, 255, 0), 0);  
            }
            frakeRestart = false;
     }
     
    ///** EXECUTE TIMER:: CHECK LOCATION **///
    if (currentMillis - previousMillis >= interval == true ) {
        Serial.println("LOQATE");
        Serial.print(currentMillis);
        Serial.println(" milliseconds");
  
        location_t loc = location.getGeoFromWiFi();
        Serial.println("Location request data");
        Serial.println(location.getSurroundingWiFiJson());
        Serial.println("Latitude: " + String(loc.lat, 7));
        Serial.println("Longitude: " + String(loc.lon, 7));
        Serial.println("Accuracy: " + String(loc.accuracy));        
        latitude = loc.lat;
        longitude = loc.lon;
    
        if (latitude >= 43.3549025)  {
            if (latitude <= 43.3661974) {
                if (longitude >= (-5.8547625)) {
                    if (longitude <= (-5.8381543)) {
                        Serial.println("BLUE BIKE INNIT!!!");
                        colorWipeReverse(Color(0, 0, 0), 1);
                        azulWipe(Color(0, 0, 255), 50);
                        inBlueMap = true;
                    } else {                        
                        colorWipeReverse(Color(0, 0, 0), 15);
                        verdeWipe(Color(0, 255, 0), 25);
                        inBlueMap = false;
                    }
                } else {
                    colorWipeReverse(Color(0, 0, 0), 15);
                    verdeWipe(Color(0, 255, 0), 25);
                    inBlueMap = false;
                }   
            } else {
                colorWipeReverse(Color(0, 0, 0), 15);
                verdeWipe(Color(0, 255, 0), 25);
                inBlueMap = false;
            } 
        } else {
            colorWipeReverse(Color(0, 0, 0), 15);
            verdeWipe(Color(0, 255, 0), 25);
            inBlueMap = false;
        }

        previousMillis = currentMillis;
 
    } 
    
    
    
    ///** INTERMITENTE RIGHT ON **///
    if (inTermitenteRight) {
        Serial.println("kappaRightON");
        naranjaRight(Color(155, 125, 0), 1);
        delay(250);

        if (frakeOn) {
             rojoFull(Color(255, 0, 0), 1);
        } else if (inBlueMap)  {
             colorWipe(Color(0, 0, 255), 1);
             delay(250);      
        } else { 
              colorWipe(Color(0, 255, 0), 1);
              delay(250);  
        }
    }
    if (inTermitenteLeft) {
        Serial.println("kappaLeftON");
        naranjaLeft(Color(155, 125, 0), 1);
        delay(250);
        if (frakeOn) {
             rojoFull(Color(255, 0, 0), 1);
        } else if (inBlueMap)  {
             colorWipe(Color(0, 0, 255), 1);
             delay(250);      
        } else { 
              colorWipe(Color(0, 255, 0), 1);
              delay(250);  
        }
    }

    /*
    if (!frakeOn) {
        rojoFull(Color(255, 0, 0), 0);   
        //Serial.println("frakeON");   
    } 
    */
    
    /*else {
        //Serial.println("frakeOFF"); 
        if (inBlueMap) {
            colorWipeReverse(Color(0, 0, 0), 1);
            azulWipe(Color(0, 0, 255), 50);
        } else {
            colorWipeReverse(Color(0, 0, 0), 15);
            verdeWipe(Color(0, 255, 0), 50);
        }
    }
    */
    //int kappaStatus = digitalRead(kappaPin); // BOTÓN INTERMITENTE

    
   // }

    // rainbow(20);
    // rainbowCycle(20);
}


///** GENERIC COLOR WIPE **///
void colorWipe(uint32_t c, uint8_t wait) {
  int i;
  for (i=0; i < strip.numPixels(); i++) {
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
}

///** GENERIC COLOR WIPE REVERSE **///
void colorWipeReverse(uint32_t c, uint8_t wait) {
  int i;
  for (i=strip.numPixels(); i > 0; i--) {
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }
}

///** AZUL WIPE **///
void azulWipe(uint32_t c, uint8_t wait) {
  int i;
  for (i=0; i < strip.numPixels(); i++) {
      if ((i == 0) || ((i >= 6) && (i <= 12))) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      } else if (i >= 18) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      }      
  }
}

///** VERDE WIPE **///
void verdeWipe(uint32_t c, uint8_t wait) {
  int i;
  for (i=0; i < strip.numPixels(); i++) {
      if ((i == 0) || ((i >= 6) && (i <= 12))) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      } else if (i >= 18) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      }      
  }
}

///** NARANJA INTERMITENTE RIGHT **///
void naranjaRight(uint32_t c, uint8_t wait) {
  int i;  
  for (i=0; i < strip.numPixels(); i++) {
      if ((i >= 9) && (i <= 15)) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      } else if ((i == 26) || (i == 25)) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      }
  }
}

///** NARANJA INTERMITENTE LEFT **///
void naranjaLeft(uint32_t c, uint8_t wait) {
  int i;  
  for (i=0; i < strip.numPixels(); i++) {
      if ((i >= 0) && (i <= 3)) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      } else if ((i >= 20) && (i <= 24)) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      }      
  }
}

///** ROJO PERIMETER **///
void rojoPerimeter(uint32_t c, uint8_t wait) {
  int i;  
  for (i=0; i < strip.numPixels(); i++) {
      if (i <= 22) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      }       
  }
}


///** ROJO FULL **///
void rojoFull(uint32_t c, uint8_t wait) {
  int i;  
  for (i=0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);      
  }
}

///** RAINBOW **///
void rainbow(uint8_t wait) {
  int i, j;
   
  for (j=0; j < 256; j++) {     // 3 cycles of all 256 colors in the wheel
    for (i=0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel( (i + j) % 255));
    }  
    strip.show();   // write all the pixels out
    delay(wait);
  }
}

///** RAINBOW CICLO **///
void rainbowCycle(uint8_t wait) {
  int i, j;
  
  for (j=0; j < 256 * 5; j++) {     // 5 cycles of all 25 colors in the wheel
    for (i=0; i < strip.numPixels(); i++) {
      // tricky math! we use each pixel as a fraction of the full 96-color wheel
      // (thats the i / strip.numPixels() part)
      // Then add in j which makes the colors go around per pixel
      // the % 96 is to make the wheel cycle around
      strip.setPixelColor(i, Wheel( ((i * 256 / strip.numPixels()) + j) % 256) );
    }  
    strip.show();   // write all the pixels out
    delay(wait);
  }
}



///** COLOR FUNCTIONS **/// 
// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b) {
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}
//Input a value 0 to 255 to get a color value.
//The colours are a transition r - g -b - back to r
uint32_t Wheel(byte WheelPos) {
  if (WheelPos < 85) {
      return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if (WheelPos < 170) {
      WheelPos -= 85;
      return Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
      WheelPos -= 170; 
      return Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}
