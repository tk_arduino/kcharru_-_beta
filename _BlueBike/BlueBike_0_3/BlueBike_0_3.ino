#include "Adafruit_WS2801.h"
#include "SPI.h" // Comment out this line if using Trinket or Gemma
#ifdef __AVR_ATtiny85__
 #include <avr/power.h>
#endif

uint8_t dataPin  = 4;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 5;    // Green wire on Adafruit Pixels

Adafruit_WS2801 strip = Adafruit_WS2801(27, dataPin, clockPin);



#ifdef ARDUINO_ARCH_SAMD
#include <WiFi101.h>
#elif defined ARDUINO_ARCH_ESP8266
#include <ESP8266WiFi.h>
#elif defined ARDUINO_ARCH_ESP32
#include <WiFi.h>
#else
#error Wrong platform
#endif 

#include <WifiLocation.h>

const char* googleApiKey = "AIzaSyAp2vRHnQol8RqyocdOYyTOxGEm3M2ucpo";
const char* ssid = "ParaisoLC";
const char* passwd = "nomepreguntes";
//const char* ssid = "FWiFi";
//const char* passwd = "riodejaneiro";

WifiLocation location(googleApiKey);

float latitude;
float longitude;

boolean checkBlueMap = false;

const int buttonPin = A0; 
int buttonState = 0; 

void setup() {

 pinMode(buttonPin, INPUT);
 
  #if defined(__AVR_ATtiny85__) && (F_CPU == 16000000L)
  clock_prescale_set(clock_div_1); // Enable 16 MHz on Trinket
#endif

  strip.begin();

  // Update LED contents, to start they are all 'off'
  strip.show();

  
    Serial.begin(115200);
    // Connect to WPA/WPA2 network
#ifdef ARDUINO_ARCH_ESP32
    WiFi.mode(WIFI_MODE_STA);
#endif
#ifdef ARDUINO_ARCH_ESP8266
    WiFi.mode(WIFI_STA);
#endif
    WiFi.begin(ssid, passwd);
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print("Attempting to connect to WPA SSID: ");
        Serial.println(ssid);
        // wait 5 seconds for connection:
        Serial.print("Status = ");
        Serial.println(WiFi.status());
        delay(500);
    }
    


}

void loop() {

 //buttonState = analogRead(buttonPin);

//int buttonState = analogRead(A0);
//int buttonState = digitalRead(D8);

buttonState = HIGH;

 if (buttonState == HIGH) {
  Serial.println("Butt");
//   if (checkBlueMap) { 
location_t loc = location.getGeoFromWiFi();

    Serial.println("Location request data");
    Serial.println(location.getSurroundingWiFiJson());
    Serial.println("Latitude: " + String(loc.lat, 7));
    Serial.println("Longitude: " + String(loc.lon, 7));
    Serial.println("Accuracy: " + String(loc.accuracy));

latitude = loc.lat;
longitude = loc.lon;

// AL NORTE    
Serial.println("Lat: " + String(latitude, 7));
if (latitude >= 43.3549025)  {
  Serial.println("Lat SOUTH: " + String(latitude, 7));
  if (latitude <= 43.3661974) {
    Serial.println("Lat NORTH: " + String(latitude, 7));
    if (longitude >= (-5.8547625)) {
      Serial.println("Lon WEST: " + String(longitude, 7));
      if (longitude <= (-5.8381543)) {
        Serial.println("Lat EAST: " + String(longitude, 7));
        Serial.println("BLUE BIKE INSIDE!!!");
        azulWipe(Color(0, 0, 255), 50);
      } else {
        colorWipe(Color(0, 0, 0), 10);
      }
    } else {
      colorWipe(Color(0, 0, 0), 10);
    }   
  } else {
    colorWipe(Color(0, 0, 0), 10);
  } 
} else {
  colorWipe(Color(0, 0, 0), 10);
}

} else {
   
/*

// AL NORTE    
Serial.println("Lat: " + String(latitude, 7));
if (latitude >= 43.3549025)  {
  Serial.println("Lat SOUTH: " + String(latitude, 7));
  if (latitude <= 43.3661974) {
    Serial.println("Lat NORTH: " + String(latitude, 7));
    if (longitude >= (-5.8547625)) {
      Serial.println("Lon WEST: " + String(longitude, 7));
      if (longitude <= (-5.8381543)) {
        Serial.println("Lat EAST: " + String(longitude, 7));
        Serial.println("BLUE BIKE INSIDE!!!");
        colorWipe(Color(0, 0, 255), 50);
        colorWipe(Color(255, 0, 0), 50);
      }
    }    
  }
}
    */
 // colorWipe(Color(255, 0, 0), 50);
 // colorWipe(Color(0, 255, 0), 50);
 // colorWipe(Color(0, 0, 255), 50);
 // rainbow(20);
 // rainbowCycle(20);

naranjaRight(Color(125, 125, 0), 1);
naranjaLeft(Color(125, 125, 0), 1);
rojoFull(Color(255, 0, 0), 1);
delay(100);

if (latitude >= 43.3549025)  {
  if (latitude <= 43.3661974) {
    if (longitude >= (-5.8547625)) {
      if (longitude <= (-5.8381543)) {
        colorWipe(Color(0, 0, 0), 1);
        delay(100);      } 
    }   
  } 
} 

naranjaRight(Color(125, 125, 0), 1);
naranjaLeft(Color(125, 125, 0), 1);
rojoFull(Color(255, 0, 0), 1);
delay(100);

if (latitude >= 43.3549025)  {
  if (latitude <= 43.3661974) {
    if (longitude >= (-5.8547625)) {
      if (longitude <= (-5.8381543)) {
        colorWipe(Color(0, 0, 0), 1);
        delay(100);
      } 
    }   
  } 
} 

naranjaRight(Color(125, 125, 0), 1);
naranjaLeft(Color(125, 125, 0), 1);
rojoFull(Color(255, 0, 0), 1);
delay(100);

if (latitude >= 43.3549025)  {
  if (latitude <= 43.3661974) {
    if (longitude >= (-5.8547625)) {
      if (longitude <= (-5.8381543)) {
        colorWipe(Color(0, 0, 0), 1);
        delay(100);
      } 
    }   
  } 

}


} 


}

// fill the dots one after the other with said color
// good for testing purposes
void colorWipe(uint32_t c, uint8_t wait) {
  int i;
  
  for (i=0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
  }
}

// fill the dots one after the other with said color
// good for testing purposes
void azulWipe(uint32_t c, uint8_t wait) {
  int i;
  
  for (i=0; i < strip.numPixels(); i++) {
      if ((i == 0) || ((i >= 6) && (i <= 12))) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      } else if (i >= 18) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      } else {
        //colorWipe(Color(0, 0, 0), 15);
      }      
  }
}

void naranjaRight(uint32_t c, uint8_t wait) {
  int i;
  
  for (i=0; i < strip.numPixels(); i++) {
      if ((i >= 9) && (i <= 15)) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      }       
  }
}

void naranjaLeft(uint32_t c, uint8_t wait) {
  int i;
  
  for (i=0; i < strip.numPixels(); i++) {
      if ((i >= 0) && (i <= 3)) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      } else if ((i >= 20) && (i <= 22)) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      }       
  }
}

void rojoFull(uint32_t c, uint8_t wait) {
  int i;
  
  for (i=0; i < strip.numPixels(); i++) {
      if (i <= 22) {
        strip.setPixelColor(i, c);
        strip.show();
        delay(wait);
      }       
  }
}



void rainbow(uint8_t wait) {
  int i, j;
   
  for (j=0; j < 256; j++) {     // 3 cycles of all 256 colors in the wheel
    for (i=0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel( (i + j) % 255));
    }  
    strip.show();   // write all the pixels out
    delay(wait);
  }
}

// Slightly different, this one makes the rainbow wheel equally distributed 
// along the chain
void rainbowCycle(uint8_t wait) {
  int i, j;
  
  for (j=0; j < 256 * 5; j++) {     // 5 cycles of all 25 colors in the wheel
    for (i=0; i < strip.numPixels(); i++) {
      // tricky math! we use each pixel as a fraction of the full 96-color wheel
      // (thats the i / strip.numPixels() part)
      // Then add in j which makes the colors go around per pixel
      // the % 96 is to make the wheel cycle around
      strip.setPixelColor(i, Wheel( ((i * 256 / strip.numPixels()) + j) % 256) );
    }  
    strip.show();   // write all the pixels out
    delay(wait);
  }
}



/* Helper functions */

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}

//Input a value 0 to 255 to get a color value.
//The colours are a transition r - g -b - back to r
uint32_t Wheel(byte WheelPos)
{
  if (WheelPos < 85) {
   return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if (WheelPos < 170) {
   WheelPos -= 85;
   return Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170; 
   return Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}
