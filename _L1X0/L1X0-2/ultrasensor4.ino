#include "Adafruit_WS2801.h"
#include "SPI.h" // Comment out this line if using Trinket or Gemma
#ifdef __AVR_ATtiny85__
 #include <avr/power.h>
#endif

/*****************************************************************************
Example sketch for driving Adafruit WS2801 pixels!


  Designed specifically to work with the Adafruit RGB Pixels!
  12mm Bullet shape ----> https://www.adafruit.com/products/322
  12mm Flat shape   ----> https://www.adafruit.com/products/738
  36mm Square shape ----> https://www.adafruit.com/products/683

  These pixels use SPI to transmit the color data, and have built in
  high speed PWM drivers for 24 bit color per pixel
  2 pins are required to interface

  Adafruit invests time and resources providing this open source code, 
  please support Adafruit and open-source hardware by purchasing 
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution

*****************************************************************************/

// Choose which 2 pins you will use for output.
// Can be any valid output pins.
// The colors of the wires may be totally different so
// BE SURE TO CHECK YOUR PIXELS TO SEE WHICH WIRES TO USE!
uint8_t dataPin  = 7;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 6;    // Green wire on Adafruit Pixels

// Don't forget to connect the ground wire to Arduino ground,
// and the +5V wire to a +5V supply

// Set the first variable to the NUMBER of pixels. 25 = 25 pixels in a row
Adafruit_WS2801 strip = Adafruit_WS2801(12, dataPin, clockPin);

// Optional: leave off pin numbers to use hardware SPI
// (pinout is then specific to each board and can't be changed)
//Adafruit_WS2801 strip = Adafruit_WS2801(25);

// For 36mm LED pixels: these pixels internally represent color in a
// different format.  Either of the above constructors can accept an
// optional extra parameter: WS2801_RGB is 'conventional' RGB order
// WS2801_GRB is the GRB order required by the 36mm pixels.  Other
// than this parameter, your code does not need to do anything different;
// the library will handle the format change.  Examples:
//Adafruit_WS2801 strip = Adafruit_WS2801(25, dataPin, clockPin, WS2801_GRB);
//Adafruit_WS2801 strip = Adafruit_WS2801(25, WS2801_GRB);

int wait1 = 10;


#include <WS2801FX.h>

#define LED_COUNT 12
#define LED_DATA_PIN 6
#define LED_CLOCK_PIN 7

#define TIMER_MS 10000

// Parameter 1 = number of pixels in strip
// Parameter 2 = data pin number
// Parameter 3 = clock pin number
// Parameter 4 (optional) = pixel type, one of WS2801_RGB/GRB/RBG
WS2801FX ws2801fx = WS2801FX(LED_COUNT, LED_DATA_PIN, LED_CLOCK_PIN, WS2801_RGB);

unsigned long last_change = 0;
//unsigned long now = 0;

unsigned long last_trigger = 0;
unsigned long now = 0;



const int EchoPin = 4;
const int TriggerPin = 3;

 int triggerRR = 0;

 
void setup() {

#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000L)
  clock_prescale_set(clock_div_1); // Enable 16 MHz on Trinket
#endif

  strip.begin();

  // Update LED contents, to start they are all 'off'
  strip.show();


/*
  ws2801fx.init();

    int i;
 
    for (i=0; i < 3; i++) {
      ws2801fx.setPixelColor(i, Wheel( (i + 0) % 255));    
    }  


  //    int i;

 
    for (i=3; i < 6; i++) {
      ws2801fx.setPixelColor(i, Wheel( (i + 42) % 255));    
    }  

 
    for (i=6; i < 9; i++) {
      ws2801fx.setPixelColor(i, Wheel( (i + 170) % 255));    
    }  

        for (i=9; i < 12; i++) {
      ws2801fx.setPixelColor(i, Wheel(85));    
    } 



     */
    /*

   // ws2801fx.setBrightness(255);
  ws2801fx.setSpeed(200);
 // ws2801fx.setColor(0x007BFF);
  ws2801fx.setMode(FX_MODE_BLINK);
  ws2801fx.start();

  */
  
   Serial.begin(9600);
   pinMode(TriggerPin, OUTPUT);
   pinMode(EchoPin, INPUT);

/*
verde();
  rojo();
  azul();
  amarillo();

*/


  

}
 
void loop() {




  
   
   int cm = ping(TriggerPin, EchoPin);
   Serial.print("Distancia: ");
   Serial.println(cm);
   delay(10);

now = millis();

//ws2801fx.service();

if (cm > 0 && cm < 50) {
/*

    ws2801fx.setMode(FX_MODE_BLINK);
  ws2801fx.start();

*/

  rojo();  
  delay(150);
  amarillo();
  delay(150);
  azul();
  delay(150);
  verde();
  delay(10000);
  colorWipe(Color(0, 0, 0), 50);

//delay(1000);
 //ws2801fx.trigger();



 /* if(now - last_change > TIMER_MS) {
    ws2801fx.setMode((ws2801fx.getMode() + 1) % ws2801fx.getModeCount());
    last_change = now;
  }*/

  
  } else {
    
  // colorWipe(Color(0, 0, 0), 50);
    
    }
  
  
 //  rainbow(20);


  


}


int ping(int TriggerPin, int EchoPin) {
   long duration, distanceCm;
   
   digitalWrite(TriggerPin, LOW);  //para generar un pulso limpio ponemos a LOW 4us
   delayMicroseconds(4);
   digitalWrite(TriggerPin, HIGH);  //generamos Trigger (disparo) de 10us
   delayMicroseconds(10);
   digitalWrite(TriggerPin, LOW);
   
   duration = pulseIn(EchoPin, HIGH);  //medimos el tiempo entre pulsos, en microsegundos
   
   distanceCm = duration * 10 / 292/ 2;   //convertimos a distancia, en cm
   return distanceCm;
}


void verde() {
  int i;
 
    for (i=6; i < 9; i++) {
      strip.setPixelColor(i, Wheel( (i + 0) % 255));    
    }  
    strip.show();   
 

}

void amarillo() {
  int i;

 
    for (i=0; i < 3; i++) {
      strip.setPixelColor(i, Wheel( (i + 45) % 255));    
    }  
    strip.show();  
}

void azul() {
  int i;

 
    for (i=9; i < 12; i++) {
      strip.setPixelColor(i, Wheel( (i + 170) % 255));    
    }  
    strip.show();  
}

void rojo() {
  int i;

 
    for (i=3; i < 6; i++) {
      strip.setPixelColor(i, Wheel(85));    
    }  
    strip.show();  
}

void negro() {
  int i;

 
    for (i=0; i < 12; i++) {
      strip.setPixelColor(i, Wheel(0));    
    }  
    strip.show();  
}

/* Helper functions */

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}

//Input a value 0 to 255 to get a color value.
//The colours are a transition r - g -b - back to r
uint32_t Wheel(byte WheelPos)
{
  if (WheelPos < 85) {
   return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if (WheelPos < 170) {
   WheelPos -= 85;
   return Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170; 
   return Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}

// fill the dots one after the other with said color
// good for testing purposes
void colorWipe(uint32_t c, uint8_t wait) {
  int i;
  
  for (i=0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
  }
}
