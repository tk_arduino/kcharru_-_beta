
byte piezoPin = 7;                // pin for piezo. Long leg of piezo: pin 4, short leg: GND.
byte buttonPin = 10;              // pin for momentary switch (between Pin 10 and GND).

void setup()
{
 pinMode(piezoPin,OUTPUT);
 pinMode(buttonPin,INPUT_PULLUP); // set up buttonPin as input with internal pull-up resistor
}

void loop()
{

   soundFX(3000.0,30+200*(1+sin(millis()/5000))); // wonky sonic screwdriver
   //soundFX(3000.0,30); // sonic screwdriver
   soundFX(100.0,30.0); // ray gun
   soundFX(3.0,30.0); // star trek
   //soundFX(1.0,30.0); // star trek high

}

void soundFX(float amplitude,float period){ 
 int uDelay=2+amplitude+amplitude*sin(millis()/period);
 for(int i=0;i<5;i++){
   digitalWrite(piezoPin,HIGH);
   delayMicroseconds(uDelay);
   digitalWrite(piezoPin,LOW);
   delayMicroseconds(uDelay);
 }
}
