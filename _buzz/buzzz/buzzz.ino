int piezo = 7;

float sinVal;
int toneVal;

void setup() {
 pinMode (piezo, OUTPUT);

   pinMode(0, OUTPUT); //LED on Model B
  pinMode(1, OUTPUT); //LED on Model A  or Pro

  
}

void loop() {
    digitalWrite(0, HIGH);   // turn the LED on (HIGH is the voltage level)
  digitalWrite(1, HIGH);
  
 tone(piezo,500,200);
 delay(200);
 tone(piezo,500,200);
 delay(200);
 tone(piezo,500,200);
 delay(200);
 tone(piezo,800,150);
 delay(150);
 tone(piezo,500,500);
 delay(500);

   digitalWrite(0, LOW);    // turn the LED off by making the voltage LOW
  digitalWrite(1, LOW); 
  
 tone(piezo,600,1000);
 delay(10000);
 
}
