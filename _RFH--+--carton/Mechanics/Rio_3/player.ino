#include "Arduino.h"
//////////////////////// PLAYER ////////////////////////
void initPlayer() {
  if (! musicPlayer.begin()) { // initialise the music player
    p(F("Couldn't find VS1053, do you have the right pins defined?"));
    //while (1);
  }
  else {
    p(F("VS1053 found"));
  }

  bool resultSD = SD.begin(CARDCS);    // initialise the SD card
  if (!resultSD) {
    p(" - - - - - - - - -    NOT SD CARD!");
  }

  // Set volume for left, right channels. lower numbers == louder volume!
  // LEFT is Speaker.   RIGHT is Handset
  setChannel('l');

  // If DREQ is on an interrupt pin (on uno, #2 or #3) we can do background
  // audio playing
  musicPlayer.useInterrupt(VS1053_FILEPLAYER_PIN_INT);  // DREQ int
}

////////////////////////// PLAY AUDIO
void playAudio(String filename, bool full) {
  p("Playing filename " + filename + " with param full: " + String(full) + ". action es " + action);
  fullPlaying = full;

  printLcd(filename, 1, "this", true);

  musicPlayer.stopPlaying();

  char charArray[filename.length()];
  filename.toCharArray(charArray, filename.length() + 1);

  musicPlayer.startPlayingFile(charArray); // Always plays not Full

  // Init elapsedQuestion
  elapsedQuestion = 0;
  
  
  //if(filename != "ring.mp3"){
     //addToStats("__ audio file" + filename); 
  //}
}

//////////////////////////   VOLUMES AND CHANNEL   /////////////////////////
void setChannel(char channel) {
  
  if (channel == 'l') {
    musicPlayer.setVolume(volumeValueLeft, 1000);
  }
  else if(channel == 'r'){
    musicPlayer.setVolume(1000, volumeValueRight);
  }
  else{ //both
    musicPlayer.setVolume(volumeValueLeft, volumeValueRight);
  }
}

void setVolumeUp() {
   volumeValueLeft = volumeValueLeft - 5;
  if (volumeValueLeft > 50) {
    // VOLUME: lower numbers LOUDER!!
     volumeValueLeft = 0;
  }
  setChannel('l');
}

void setVolumeDown() {
   volumeValueLeft = volumeValueLeft + 5;
  if (volumeValueLeft > 50) {
    // VOLUME: lower numbers LOUDER!!
     volumeValueLeft = 50;
  }
  setChannel('l');
}


void beep() {
  digitalWrite(53, HIGH);
  delay(100);
  digitalWrite(53, LOW);
}

