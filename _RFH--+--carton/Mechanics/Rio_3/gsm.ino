#include <SoftwareSerial.h>

char incoming_char=0;      //Will hold the incoming character from the Serial Port.

SoftwareSerial cell(10,11); 

bool gsmInitialised = false;
bool gsmReady = false;
bool readyForMessage = false;

bool sind3 = false;
bool sind4 = false;
bool sind11 = false;

char SIND_3[] = "+SIND: 3";
char SIND_4[] = "+SIND: 4";
char SIND_11[] = "+SIND: 11";

char crtlZ = 0x1A;
char carriageReturn = 0x0D;
char roof = '^';

char atSetTextMode[] = "AT+CMGF=1";
char setSmsNumber[] = "AT+CMGS=";


void readGsmSerial()
{
  if(cell.available() > 0) 
  {
   String response;
   bool responseCompleted = false;
    
    while(cell.available() >0)
    {
      
        incoming_char=cell.read();    //Get the character from the cellular serial port.
        
        if(incoming_char == carriageReturn)
        {
            responseCompleted = true;
            break;
        }
        
        if(!responseCompleted)
        {
          response += incoming_char;
        }   
     }
     if(response.length() > 0)
     {
       
       if(!gsmReady)
       {
          if(!sind3 && response.indexOf(SIND_3) > -1)
          {
             sind3 = true;
          }
          
          if(!sind4 && response.indexOf(SIND_4) > -1)
          {
             sind4 = true;
          }
          
          if(!sind11 && response.indexOf(SIND_11) > -1)
          {
             sind11 = true;
          }
          
          if(sind4 && sind3 && sind11)
          {
              gsmReady = true;
             
              Serial.println("gsm initialised 100%");
              setTextMode();
          }
       }
       
       if(testMode)
       {
          Serial.print(response);
       }  
     }
  } 
}

void initGSM() {
  
  if(!gsmInitialised)
  {
      cell.begin(9600);
      p(F("Initialising gsm"));
      gsmInitialised = true;
      gsmReady = false;
  }
}

unsigned long time;

void sendSMS(String txtMsgStr, String remoteNumberStr) {
  
  time = micros();
  txtMsgStr = txtMsgStr + "   " + String(time);
  
  p("sendSMS with text " + txtMsgStr + " to the number " + remoteNumberStr);

  if (!gsmInitialised) {
    p(F("GSM notConnected"));
    return;
  }

  char remoteNumber[20];
  remoteNumberStr.toCharArray(remoteNumber, remoteNumberStr.length() + 1);

  char txtMsg[200];
  txtMsgStr.toCharArray(txtMsg, txtMsgStr.length() + 1);
    
  printToSerialAndGsm(setSmsNumber);
  printToSerialAndGsm("\"");
  printToSerialAndGsm(remoteNumber);
  printToSerialAndGsm("\"");
  printToSerialAndGsm(carriageReturn);
 
  delay(1000);
  
  printToSerialAndGsm(txtMsg);
  printToSerialAndGsm(crtlZ);
  
}

void printToSerialAndGsm(char message[])
{
  Serial.print(message);
  cell.print(message);
}
void printToSerialAndGsm(char ch)
{
  if(testMode)
  {
    Serial.print(ch);
  }
  cell.print(ch);
}

void setTextMode()
{
  printToSerialAndGsm(atSetTextMode);
  printToSerialAndGsm(carriageReturn);
}
