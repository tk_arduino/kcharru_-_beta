#include "Arduino.h"


// Code for Statistics, not implemented
File myFile;

unsigned long initTime;
unsigned long currentTime;
unsigned long duration;

String statsText;

void startStats() {
  
  initTime = millis();
  //p("initTime " + String(initTime) + ". In secs " + String(initTime / 1000));
  currentTime = (initTime / 1000) + startUnix;
  statsText = statsText + "--START-- <Date>" + String(currentTime) + "</Date>";
  p(F("startStats: ")); 
  p(statsText);
  
}

void addToStats(String text) {
  
  statsText = statsText + text;
  p(F("addToStats: ")); p(statsText);
  
}

void endStats() {
  
  duration = (millis() - initTime) / 1000;
  
  statsText = statsText + "--END. Duration " + String(duration) + ". FreeMemory " + String(freeMemory());
  p(F("endStats: ")); p(statsText);
  writeToFile(statsText);
}

void writeToFile(String text) {
  /*
  // check is not playing music
  if(musicPlayer.playingMusic){
    p(F(" > > Trying to write on the SD, not possible, file is playing"));
     return; 
  }
  
 
  // Not needed, but keep it just in case
  //char charArray[filename.length()];
  //filename.toCharArray(charArray, filename.length() + 1);
  
  myFile = SD.open("L/stats.txt", FILE_WRITE);
  if (myFile) {
    p(F("Writing to SD: "));  p(text);
    myFile.println(" ");
    myFile.println(text);
    myFile.close();
    
    statsText = "";
  } else {
    p(F("error opening stats.txt"));
  }
  */
}
