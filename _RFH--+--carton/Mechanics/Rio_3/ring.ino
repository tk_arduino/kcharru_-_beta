#include "Arduino.h"


/////// ///////  PRESENCE 

void setDetectPresence() {
  p(F("in setDetectPresence"));
  if (!isPlayingMechanic) {
    p(F("setDetectPresence = true"));
    detectPresence = true;
  }
  else {
    p(F("setDetectPresence NOT set to true because isPlayingMechanic!"));
  }
}

void checkPresence() {
  
  if (detectPresence && isColgado) {

    unsigned int uS = sonar.ping(); // Send ping, get ping time in microseconds (uS).
    unsigned int cm = uS / US_ROUNDTRIP_CM;

    isPresence = (cm != 0 && cm > 12 && cm < 80);
    //p(String(cm) + " cm. isPresence " + String(isPresence));

    if (isPresence) {
      if (presenceCount > 1) {
        p(F("presenceDetected -> startRinging"));
        presenceCount = 0;
        startRinging(false);
      }
      else {
        presenceCount++;
      }
    }
    else {
      presenceCount = 0;
    }
    delay(100);
  }

}

/////////////////////// RING ///////////////////
void startRinging(bool isRandom) {
  p(F("StartRinging"));
  // Do not Ring if: 
  if (isRinging || !isColgado || isPlayingMechanic || !detectPresence) {
    p("do not Ring. isRinging " + String(isRinging) + " isColgado " + String(isColgado) + " isPlayingMechanic " + String(isPlayingMechanic) + " detectPresence " +  String( detectPresence));
    return;
  }
  
  addToStats(" R ");

  setChannel('b'); // BOTH
  if (isRandom && testMode) {
    playAudio("ringram.mp3", false);
  }
  else {
    playAudio("ring.mp3", false);
  }

  isRinging = true;
  timerStopRinging = simpleTimer.setTimeout(20000, stopRinging);
}

void stopRinging() {
  if (!isRinging) {
    return;
  }
  p(F("Stop Ringing"));
  isRinging = false;
  musicPlayer.stopPlaying();
}


void randomRing() {

  timerRandom = simpleTimer.setTimeout(randomRingInterval, randomRing);
  p(F("randomRing"));
  if (isPlayingMechanic || isRinging) {
    p("Do not execute randomRing. isPlayingMechanic " + String(isPlayingMechanic) + " y  isRinging " + String(isRinging));
    return;
  }
  bool doit = rio.getRandomRing();

  p(F("doit in RandomRing (plays) is ")); p(String(doit) );
  if (doit) {
    startRinging(true);
  }
}
