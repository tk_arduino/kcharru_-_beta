#include "Arduino.h"

// LCD
String text1;
String text2;
int timerShowMessage;
int timerShowMessage2;
unsigned long showMessageDelay;

//////////////////////// LCD   ////////////////////////
void printLcd(String text, int line, String mustClear, bool justTest) {

  if (justTest && !testMode) {
    return;
  }
  text = " " + text; // Add a space at the beginning
  if (mustClear == "both") {
    lcd.clear();
  }

  lcd.setCursor(0, line);

  if (mustClear == "this") {
    lcd.print("                   ");
    lcd.setCursor(0, line);
  }

  lcd.print(text);
}

void showMessage() {
  printLcd(text1, 0, "both", false);
  printLcd(text2, 1, "none", false);
}

void printQuestion(char choice) {

  simpleTimer.deleteTimer(timerShowMessage);
  simpleTimer.deleteTimer(timerShowMessage2);
  showMessageDelay = 0;
  text1 = "";
  text2 = "";
  
  String type = "";
  String brazilianName = "";
  int question = questionsCount;


  // It IS an ANSWER!!
  if (choice != ' ') {

    if (question == 1) {
      type = "ageNotAllowed";
    }
    else if (question == 11) {
      type = "notBad";
    }
    else if (question == 23) {

      if (playingFileName.indexOf('a') != -1) {
        type = "brazilianName";
        brazilianName = "Mariana!"; // Carvalho
      }
      if (playingFileName.indexOf('b') != -1) {
        type = "brazilianName";
        brazilianName = "Jussara Lima!"; //
      }
      if (playingFileName.indexOf('c') != -1) {
        type = "brazilianName";
        brazilianName = "Raimunda!"; // Almeida
      }
      if (playingFileName.indexOf('d') != -1) {
        type = "brazilianName";
        brazilianName = "Fabiano Barbosa"; //
      }
      if (playingFileName.indexOf('e') != -1) {
        type = "brazilianName";
        brazilianName = "Henrique!"; // Cardoso
      }
      if (playingFileName.indexOf('f') != -1) {
        type = "brazilianName";
        brazilianName = "Jorge De Silva!"; //
      }

    }
    else if (questionsCount == 25 && choice == '2') {
      type = "press1";
      showMessageDelay = 2000;
    }
    else {
      type = "clear";
    }
  }
  else {
    // Questions
    switch (question) {
      case 1:
        type = "age";
        break;

      case 5:
      case 12:
      case 24:
        type = "clear";
        break;


      case 22:
        type = "name";
        break;

      case 23:
        type = "spinning";
        break;


      default:
        type = "star";

        break;
    }
  }

  if (type == "clear") {
    text1 = "";
    text2 = "";
  }
  if (type == "star") {
    text1 = "To repeat the";
    text2 = "question press*";
  }
  if (type == "age") {
    text1 = "Enter your age";
    text2 = "followed #";
  }
  if (type == "ageNotAllowed") {
    text1 = "Sorry 14+ ONLY";
  }
  if (type == "notBad") {
    text1 = "Not bad!!";
    text2 = "Getting there!!";
  }
  if (type == "name") {
    text1 = "1 = female name";
    text2 = "2 = male name";
  }
  if (type == "spinning") {
    text1 = "Spinning";
    text2 = "the wheel...";
  }
  if (type == "brazilianName") {
    text1 = "Congratulations";
    text2 = brazilianName;
  }
  if (type == "vivaBrasil") {
    text1 = "Viva Brasil";
  }
  if (type == "press1") {
    text1 = "Press 1 Press 1";
  }

  
  timerShowMessage = simpleTimer.setTimeout(showMessageDelay, showMessage);

}

void setBacklight(String state) {
  if (state == "on") {
    digitalWrite(BL1, HIGH);
  }
  else {
    digitalWrite(BL1, LOW);
    printLcd("", 0, "both", false);
  }
}
