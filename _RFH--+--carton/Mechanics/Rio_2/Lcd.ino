#include "Arduino.h"

// LCD
String text1;
String text2;
int timerShowMessage;

//////////////////////// LCD   ////////////////////////
void printLcd(String text, int line, String mustClear, bool justTest) {
  if (justTest && !testMode) {
    return;
  }

  text = " " + text; // Add a space at the beginning
  if (mustClear == "both") {
    lcd.clear();
  }

  lcd.setCursor(0, line);

  if (mustClear == "this") {
    lcd.print("                   ");
    lcd.setCursor(0, line);
  }

  lcd.print(text);
}



void printSpecial() {

  String text1 = "";
  String text2 = "";


  if (lcdMessage == "welcome") {
    text1 = "Welcome to Five";
    text2 = "min Meditation";
  }
  if (lcdMessage == "breathe") {
    text1 = "Breathe + Relax";
  }
  if (lcdMessage == "finger") {
    text1 = "Place Finger on";
    text2 = "the Heart";
  }
  if (lcdMessage == "heartbeat") {
    if(globalBeats == 0){
       globalBeats = 85; 
    }
    text1 = String(globalBeats) + " BPM";
  }
  if (lcdMessage == "end") {
    text1 = "Thank you for";
    text2 = "listening. Bye";
  }


  printLcd(text1, 0, "both", false);
  printLcd(text2, 1, "none", false);


}

void setBacklight(String state) {
  if (state == "on") {
    digitalWrite(BL1, HIGH);
  }
  else {
    digitalWrite(BL1, LOW);
    printLcd("", 0, "both", false);
  }
}
