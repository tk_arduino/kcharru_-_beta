
//  Variables
int pulsePin = A0;                 // Pulse Sensor purple wire connected to analog pin 0
int blinkPin = 13;                // pin to blink led at each beat
int fadePin = 1;                  // pin to do fancy classy fading blink at each beat
int fadeRate = 0;                 // used to fade LED on with PWM on fadePin

// Volatile Variables, used in the interrupt service routine!
volatile int BPM;                   // int that holds raw Analog in 0. updated every 2mS
volatile int Signal;                // holds the incoming raw data
volatile int IBI = 600;             // int that holds the time interval between beats! Must be seeded! 
volatile boolean Pulse = false;     // "True" when User's live heartbeat is detected. "False" when not a "live beat". 
volatile boolean QS = false;        // becomes true when Arduoino finds a beat.

// Regards Serial OutPut  -- Set This Up to your needs
static boolean serialVisual = false;   // Set to 'false' by Default.  Re-set to 'true' to see Arduino Serial Monitor ASCII Visual Pulse 



volatile int rate[10];                    // array to hold last ten IBI values
volatile unsigned long sampleCounter = 0;          // used to determine pulse timing
volatile unsigned long lastBeatTime = 0;           // used to find IBI
volatile int P =512;                      // used to find peak in pulse wave, seeded
volatile int T = 512;                     // used to find trough in pulse wave, seeded
volatile int thresh = 525;                // used to find instant moment of heart beat, seeded
volatile int amp = 100;                   // used to hold amplitude of pulse waveform, seeded
volatile boolean firstBeat = true;        // used to seed rate array so we startup with reasonable BPM
volatile boolean secondBeat = false;      // used to seed rate array so we startup with reasonable BPM

#define BPM_ARRAY_SIZE 5
#define MAX_BPM_ALLLOWED_DIFFERENCE 2
int bpms[BPM_ARRAY_SIZE];
int bpmIndex = 0;


void interruptSetup(){     
  // Initializes Timer2 to throw an interrupt every 2mS.
  TCCR2A = 0x02;     // DISABLE PWM ON DIGITAL PINS 3 AND 11, AND GO INTO CTC MODE
  TCCR2B = 0x06;     // DON'T FORCE COMPARE, 256 PRESCALER 
  OCR2A = 0X7C;      // SET THE TOP OF THE COUNT TO 124 FOR 500Hz SAMPLE RATE
  TIMSK2 = 0x02;     // ENABLE INTERRUPT ON MATCH BETWEEN TIMER2 AND OCR2A
  sei();             // MAKE SURE GLOBAL INTERRUPTS ARE ENABLED      
} 


// THIS IS THE TIMER 2 INTERRUPT SERVICE ROUTINE. 
// Timer 2 makes sure that we take a reading every 2 miliseconds
ISR(TIMER2_COMPA_vect){                         // triggered when Timer2 counts to 124
  cli();                                      // disable interrupts while we do this
  Signal = analogRead(pulsePin);              // read the Pulse Sensor 
  sampleCounter += 2;                         // keep track of the time in mS with this variable
  int N = sampleCounter - lastBeatTime;       // monitor the time since the last beat to avoid noise

    //  find the peak and trough of the pulse wave
  if(Signal < thresh && N > (IBI/5)*3){       // avoid dichrotic noise by waiting 3/5 of last IBI
    if (Signal < T){                        // T is the trough
      T = Signal;                         // keep track of lowest point in pulse wave 
    }
  }

  if(Signal > thresh && Signal > P){          // thresh condition helps avoid noise
    P = Signal;                             // P is the peak
  }                                        // keep track of highest point in pulse wave

  //  NOW IT'S TIME TO LOOK FOR THE HEART BEAT
  // signal surges up in value every time there is a pulse
  if (N > 250){                                   // avoid high frequency noise
    if ( (Signal > thresh) && (Pulse == false) && (N > (IBI/5)*3) ){        
      Pulse = true;                               // set the Pulse flag when we think there is a pulse
//      digitalWrite(blinkPin,HIGH);                // turn on pin 13 LED
      IBI = sampleCounter - lastBeatTime;         // measure time between beats in mS
      lastBeatTime = sampleCounter;               // keep track of time for next pulse

      if(secondBeat){                        // if this is the second beat, if secondBeat == TRUE
        secondBeat = false;                  // clear secondBeat flag
        for(int i=0; i<=9; i++){             // seed the running total to get a realisitic BPM at startup
          rate[i] = IBI;                      
        }
      }

      if(firstBeat){                         // if it's the first time we found a beat, if firstBeat == TRUE
        firstBeat = false;                   // clear firstBeat flag
        secondBeat = true;                   // set the second beat flag
        sei();                               // enable interrupts again
        return;                              // IBI value is unreliable so discard it
      }   


      // keep a running total of the last 10 IBI values
      word runningTotal = 0;                  // clear the runningTotal variable    

      for(int i=0; i<=8; i++){                // shift data in the rate array
        rate[i] = rate[i+1];                  // and drop the oldest IBI value 
        runningTotal += rate[i];              // add up the 9 oldest IBI values
      }

      rate[9] = IBI;                          // add the latest IBI to the rate array
      runningTotal += rate[9];                // add the latest IBI to runningTotal
      runningTotal /= 10;                     // average the last 10 IBI values 
      BPM = 60000/runningTotal;               // how many beats can fit into a minute? that's BPM!
      QS = true;                              // set Quantified Self flag 
      // QS FLAG IS NOT CLEARED INSIDE THIS ISR
    }                       
  }

  if (Signal < thresh && Pulse == true){   // when the values are going down, the beat is over
//    digitalWrite(blinkPin,LOW);            // turn off pin 13 LED
    Pulse = false;                         // reset the Pulse flag so we can do it again
    amp = P - T;                           // get amplitude of the pulse wave
    thresh = amp/2 + T;                    // set thresh at 50% of the amplitude
    P = thresh;                            // reset these for next time
    T = thresh;
  }

  if (N > 2500){                           // if 2.5 seconds go by without a beat
    thresh = 512;                          // set thresh default
    P = 512;                               // set P default
    T = 512;                               // set T default
    lastBeatTime = sampleCounter;          // bring the lastBeatTime up to date        
    firstBeat = true;                      // set these to avoid noise
    secondBeat = false;                    // when we get the heartbeat back
  }

  sei();                                   // enable interrupts when youre done!
}// end isr

bool sensorStarted = false;

void setupSensor(){
   pinMode(A2, OUTPUT);
   pinMode(A3, OUTPUT);
  
  interruptSetup();                 // sets up to read Pulse Sensor signal every 2mS  
}

int pulseCounter = 0;
int beatSeed;

void checkSensor()
{
  if(!sensorStarted){
    return;
  }
  
  if(QS == true)
  {
    QS = false;

    if(BPM > 60 && BPM < 130)
    {
      if(bpmIndex < BPM_ARRAY_SIZE)
      {
        bpms[bpmIndex] = BPM;
        bpmIndex++;
      }
      else
      {
        for(int i = 1; i < BPM_ARRAY_SIZE; i++)
        {
          bpms[i-1] = bpms[i];
        }
        
        bpms[BPM_ARRAY_SIZE-1] = BPM;
        
        if(checkIfAllBpmsAreSimilar())
        {
          p(F("pulseDetected"));
           musicPlayer.stopPlaying();
           delay(100);
           pulseDetected();
           
        }
      }
    }
    else 
    {
      Serial.print("too high / low bpm: ");
      Serial.println(BPM);
    }
      
  }
 
  for(int i = 0; i < BPM_ARRAY_SIZE; i++)
  {
     Serial.print(bpms[i]);
     Serial.print(" ");
  }
  
  Serial.println();
 
  delay(200);
}

bool checkIfAllBpmsAreSimilar()
{
  
   int i;
   int bpmSum = 0;
   
   for (i = 0; i < BPM_ARRAY_SIZE; i++)
   {
     bpmSum += bpms[i];  
   }
   
  int averageBpm =  bpmSum / BPM_ARRAY_SIZE;
  
  Serial.print("average: ");
  Serial.println(averageBpm);
  
  for(int i = 0; i < BPM_ARRAY_SIZE; i++)
  {
    if(abs(bpms[i] - averageBpm) > MAX_BPM_ALLLOWED_DIFFERENCE)
    {
        return false;
    }
  }
  
  return true;
}

void lcdBeat(int beats){
        
   if(beats < 65|| beats > 140){
      beats = beatSeed;
   }
   beats = rio.getRandom(5) + beats; 
   globalBeats = beats;
   printLcd(String(beats) + " BPM", 0, "both", false);
   delay(100);
}

void startSensor(){
  interruptSetup();
  globalBeats = 0;
  p(F("startSensor"));
  beatSeed = rio.getRandom(31) + 70;
  sensorStarted = true;
  analogWrite(A1, 255);
 // analogWrite(A3, 255);
  bpmIndex = 0;
  
  for(int i = 0; i < BPM_ARRAY_SIZE; i++)
  {
     bpms[i] = 0;
  }
 
  lcdMessage = "finger";
  printSpecial();
}

void stopSensor(){
  p(F("stopSensor"));
  sensorStarted = false;
  analogWrite(A1, 0);
 // analogWrite(A3, 0);
}

void pulseDetected(){
  isFingerDetected = true;
  p(F("pulseDetected, call nextQuestion"));
  nextQuestion();
}


