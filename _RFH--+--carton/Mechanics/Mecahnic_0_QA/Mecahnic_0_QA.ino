/***************************************************
  This is an example for the Adafruit VS1053 Codec Breakout

  Designed specifically to work with the Adafruit VS1053 Codec Breakout
  ----> https://www.adafruit.com/products/1381

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ****************************************************/

// include SPI, MP3 and SD libraries
#include <SPI.h>
#include <Adafruit_VS1053.h>
#include <SD.h>

// define the pins used
#define CLK 52       // SPI Clock, shared with SD card
#define MISO 50      // Input data, from VS1053/SD card
#define MOSI 51      // Output data, to VS1053/SD card
// Connect CLK, MISO and MOSI to hardware SPI pins.
// See http://arduino.cc/en/Reference/SPI "Connections"

// These are the pins used for the breakout example
#define BREAKOUT_RESET  9      // VS1053 reset pin (output)
#define BREAKOUT_CS     10     // VS1053 chip select pin (output)
#define BREAKOUT_DCS    8      // VS1053 Data/command select pin (output)
// These are the pins used for the music maker shield
#define SHIELD_RESET  -1      // VS1053 reset pin (unused!)
#define SHIELD_CS     7      // VS1053 chip select pin (output)
#define SHIELD_DCS    6      // VS1053 Data/command select pin (output)

// These are common pins between breakout and shield
#define CARDCS 4     // Card chip select pin
// DREQ should be an Int pin, see http://arduino.cc/en/Reference/attachInterrupt
#define DREQ 2       // VS1053 Data request, ideally an Interrupt pin

Adafruit_VS1053_FilePlayer musicPlayer =
  // create breakout-example object!
Adafruit_VS1053_FilePlayer(BREAKOUT_RESET, BREAKOUT_CS, BREAKOUT_DCS, DREQ, CARDCS);
// create shield-example object!
//Adafruit_VS1053_FilePlayer(SHIELD_RESET, SHIELD_CS, SHIELD_DCS, DREQ, CARDCS);


// General Variables
String playingFileName;

bool isRinging = false;
bool isPlayingMechanic = false;
bool isPlayingSound = false;


// -DEV
bool heartBeat = false;

int ledPin = 13;
int knockSensor = 0;               
byte val = 0;
int statePin = LOW;
int THRESHOLD = 100;

 



int questionsCount = 0;
const int numberOfQuestions = 20;
int choices[numberOfQuestions];

String questions[numberOfQuestions][5] = {
  {"nothingHere", "nothingAnswer", "0", "0", "0"},
  
  {"001a.mp3", "001b.mp3", "001a.mp3", "001b.mp3", "001a.mp3"},
  {"002a.mp3", "002b.mp3", "002c.mp3", "002d.mp3", "002e.mp3"},
  // 3 4 5 6    TRUE FALSE  - -   NOT AWNSER
  {"True", "False", "notAllowed", "notAllowed", "notAllowed"},
  {"True", "False", "notAllowed", "notAllowed", "notAllowed"},
  {"True", "False", "notAllowed", "notAllowed", "notAllowed"},
  {"True", "False", "notAllowed", "notAllowed", "notAllowed"},
  // 7 8 9   2 options    - -   NOT AWNSER
  {"True", "False", "notAllowed", "notAllowed", "notAllowed"},
  {"True", "False", "notAllowed", "notAllowed", "notAllowed"},
  {"True0", "False", "notAllowed", "notAllowed"},
  // 10   -  5 options BUT same Answer
  {"010a.mp3", "010a.mp3", "010a.mp3", "010a.mp3", "010a.mp3"},
  // 11  NOT ANSWER
  {"0", "0", "0", "0", "0"},
  // 12 -> 18  NOT  QUESTIONS!!!   CONDITIONATED  ANSWERS BASED ON   3 -> 9
  {"conditionated", "3", "012a.mp3", "012b.mp3", "0"},
  {"conditionated", "4", "013a.mp3", "013b.mp3", "0"},
  {"conditionated", "5", "014a.mp3", "014b.mp3", "0"},
  {"conditionated", "6", "015a.mp3", "015b.mp3", "0"},
  {"conditionated", "7", "016a.mp3", "016b.mp3", "0"},
  {"conditionated", "8", "017a.mp3", "017b.mp3", "0"},
  {"conditionated", "9", "018a.mp3", "018b.mp3", "0"},
  // 19 Also conditionated, same as before but with 5 options. Related to question 10
  {"conditionated", "10", "019a.mp3", "019b.mp3", "0"}
};



// 
void startRinging() {
  isRinging = true;
  
  playingFileName = "ring.mp3";
  playAudio(playingFileName, false);
  
  //delay(2000);
  
  //nextQuestion();
}

void startMechanic(){
  p("Starting Mechanic");
  isPlayingMechanic = true; 
  nextQuestion();
}

//- PLAYER
String folder = "0/";

int volumeValue = 30;
void initPlayer() {
  if (! musicPlayer.begin()) { // initialise the music player
    Serial.println(F("Couldn't find VS1053, do you have the right pins defined?"));
    while (1);
  }
  Serial.println(F("VS1053 found"));

  SD.begin(CARDCS);    // initialise the SD card
  // Set volume for left, right channels. lower numbers == louder volume!
  musicPlayer.setVolume(volumeValue, volumeValue);

  // If DREQ is on an interrupt pin (on uno, #2 or #3) we can do background
  // audio playing
  musicPlayer.useInterrupt(VS1053_FILEPLAYER_PIN_INT);  // DREQ int
}

void playAudio(String filename, bool full) {
  musicPlayer.stopPlaying();

  char charArray[filename.length()];
  filename.toCharArray(charArray, filename.length() + 1);

  if(full){
     musicPlayer.playFullFile(charArray);
  }
  else{
    musicPlayer.startPlayingFile(charArray);
  }
 
  p("playing filename " + filename);
  delay(200);

}

void checkIsPlaying(){
    isPlayingSound = musicPlayer.stopped();
    
    if(isPlayingSound){
      //p("STOPPED!! ");
    }
    else{
      //p("NOOOOt stopped ");
    }
}

void setVolumeUp() {
  if (volumeValue > 1) {
    // VOLUME: lower numbers LOUDER!!
    volumeValue = volumeValue - 5;
  }l
  musicPlayer.setVolume(volumeValue, volumeValue);
}

void setVolumeDown() {
  if (volumeValue < 50) {
    volumeValue = volumeValue + 5;
  }
  musicPlayer.setVolume(volumeValue, volumeValue);
}

void beep(){
  //tone(pin, frequency, duration)
  musicPlayer.sineTest(0x44, 500);
}




//- QUESTIONS /////////////////////////

String getQuestionFileName() {
  
  String prefix;
  if (questionsCount < 10) {
    prefix = "00";
  }
  else {
    prefix = "0";
  }

  String name = folder  + prefix + String(questionsCount) + ".mp3";
  return name;
}

void nextQuestion() {
  questionsCount++;
  if(questionsCount == numberOfQuestions){
     return; 
  }
  // Check if it has a question
  String firstValue = questions[questionsCount][0];

  if (firstValue != "conditionated") {
    playingFileName = getQuestionFileName();
    playAudio(playingFileName, false);
  }
  else {
    p("Conditionated!!");
    int relatedQuestion = questionsCount - 9;
    int userChoice = choices[relatedQuestion];

    String choiceLetter;

    if (userChoice == 1) {
      choiceLetter = "a";
    }
    if (userChoice == 2) {
      choiceLetter = "b";
    }
    if (userChoice == 3) {
      choiceLetter = "c";
    }
    if (userChoice == 4) {
      choiceLetter = "d";
    }
    if (userChoice == 5) {
      choiceLetter = "e";
    }

    playingFileName = folder + "0" + String(questionsCount) + choiceLetter + ".mp3";

    Serial.println(playingFileName);
    playAudio(playingFileName, true);
  }
  
  if(questionsCount > 10){
     nextQuestion(); 
  }

}
void repeatQuestion() {
  playAudio(playingFileName, false);
}


// Options
void processChoice(char choice) {
  
  int convertedChoice = choice - '0';//int(choice);
  choices[questionsCount] = convertedChoice;
  convertedChoice--;
  
  if(convertedChoice > 4){
     return; 
  }


  String answerAudio = questions[questionsCount][convertedChoice];
 p("AnswerAudio " + answerAudio);
  if(answerAudio == "notAllowed"){
    p("NOT ALLOWED! Must return");
    return;
  }
  
  playingFileName = folder + answerAudio;
  Serial.println(playingFileName);
  
  if(answerAudio != "0"){
      playAudio(playingFileName, true);
  }
  
  nextQuestion();

}

//- HELPERS
void p(String text) {
  Serial.println(text);
}

String heartFilename = "heart.mp3";

void setup() {
  Serial.begin(9600);
  
  if(heartBeat){
    pinMode(ledPin, OUTPUT); 
    initPlayer();
    
    playAudio(heartFilename, false);
  }
  else{
    
    initProximitySensor();
    initLcd();
    initPlayer();
    //startRinging();
  }
}

void readSerial() {
  if (Serial.available() > 0)
  {
    char c = Serial.read();

    keyReceived(c);

  }
}

//- Keyborad
#include <Keypad.h>

const byte ROWS = 4; //four rows
const byte COLS = 4; //three columns

// 22 - 29
const int pinUp1   = 23; 
const int pinDown1 = 22;
const int pinUp2   = 25;
const int pinDown2 = 24;
const int pinUp3   = 27;
const int pinDown3 = 26;
const int pinUp4   = 29;
const int pinDown4 = 28;


char keys[ROWS][COLS] = {
  {'A', '1', '2', '3'},
  {'B', '4', '5', '6'},
  {'C', '7', '8', '9'},
  {'D', '*', '0', '#'}
};

byte rowPins[ROWS] = {pinDown1, pinDown2, pinDown3, pinDown4}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {pinUp1, pinUp2, pinUp3, pinUp4}; //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );





//- PROXIMITY SENSOR  //////////////////////////////////////////////////////

int trigPin = 33;    //Trig - green Jumper
int echoPin = 32;    //Echo - yellow Jumper
long duration, cm, inches;

void initProximitySensor(){
  //Define inputs and outputs
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

// Testing
bool showDistance = true;

void checkProximity(){
  
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
    // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
    digitalWrite(trigPin, LOW);
    delayMicroseconds(5);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);

    // Read the signal from the sensor: a HIGH pulse whose
    // duration is the time (in microseconds) from the sending
    // of the ping to the reception of its echo off of an object.
    pinMode(echoPin, INPUT);
    duration = pulseIn(echoPin, HIGH);

    // convert the time into a distance
    cm = (duration / 2) / 29.1;

    detectPresence(cm);

    String stringOne = " cm";
    String stringThree = cm + stringOne;


    if (showDistance) {
      printLcd(stringThree);
    }
}

bool detectPresence(long cm) {
  if (cm < 50) {
    if(!isPlayingMechanic && !isRinging){
      startRinging();
    }
    //p("Somebody is next to the phone!");
  }
}


//- LCD   //////////////////////////////////////////////////////
#include <LiquidCrystal.h>

const int RS = 41;
const int EN = 39;
//const int RW = 8;

const int DB4 = 38;
const int DB5 = 40;
const int DB6 = 42;
const int DB7 = 44;

LiquidCrystal lcd(RS, EN, DB4, DB5, DB6, DB7);


void initLcd() {
  
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  printLcd("Intimatron");
}

void printLcd(String text) {
  lcd.clear();
  lcd.print(text);
}




void readKeypad(){
  char key = keypad.getKey();
  
  if (key){
    //beep();
    keyReceived(key);
    
    //p(String(key));
  }
}

void keyReceived(char c){
  
  p(String(c));
  
  if (c == '1' || c == '2' || c == '3' || c == '4' || c == '5' ||
        c == '6' || c == '7' || c == '8' || c == '9') {
      processChoice(c);
    }
    if (c == '0' && !isPlayingMechanic) {
      startMechanic();
      //showDistance = !showDistance;
    }
    
    if (c == '#') {
      repeatQuestion();
    }

    if (c == 'n') {
      nextQuestion();
    }

    // VOLUME
    if (c == 'A') {
      setVolumeUp();
    }
    if (c == 'B') {
      setVolumeDown();
    }
    
    // Control Player
    if (c == 's') {

      startRinging();
    }

    if (c == 'p') {
      if (! musicPlayer.paused()) {
        Serial.println("Paused");
        musicPlayer.pausePlaying(true);
      } else {
        Serial.println("Resumed");
        musicPlayer.pausePlaying(false);
      }
    }
}


bool heartStarted = true;
void loop() {
  if(heartBeat){
//p("Checking Heart");
    val = analogRead(knockSensor);     

    if (val >= THRESHOLD) {
      statePin = !statePin;
      digitalWrite(ledPin, statePin);
      Serial.println("Knock!");
      
      musicPlayer.pausePlaying(false);
      
      //
    }
    else{
      musicPlayer.pausePlaying(true);
    }
    
    delay(100);  // we have to make a delay to avoid overloading the serial port
  }
  else{
    // File is playing in the background
    if (musicPlayer.stopped()) {
      //Serial.println("Done playing music");
      //while (1);
    }
    checkProximity();
    readSerial();
    readKeypad();
    checkIsPlaying();
    delay(100);
  }
}
