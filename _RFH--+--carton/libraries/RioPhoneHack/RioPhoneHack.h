#ifndef RioPhoneHack_H
#define RioPhoneHack_H

#include "Arduino.h"

// LCD 
// This is included in the Sketch
// #include "LiquidCrystal.h"
// REFERENCE: http://www.arduino.cc/en/Reference/LiquidCrystal
const int RS = 41;
const int EN = 39;
//const int RW = 8;

const int DB4 = 40;
const int DB5 = 37;
const int DB6 = 38;
const int DB7 = 35;

const int BL1 = 36;

// This is included in the Sketch
// LiquidCrystal lcd(RS, EN, DB4, DB5, DB6, DB7);


// PROXIMITY SENSOR
const int echoPin = 31;    //Echo 
const int trigPin = 33;    //Trig 

#define MAX_DISTANCE 400
#define TRIGGER_PIN  33  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN     31
/*
#define TRIGGER_PIN1  33  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN1     31  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE1 400   // NewPing Lib
*/

#define TRIGGER_PIN2  33  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN2     31  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE2 400   // NewPing Lib
/*

#define TRIGGER_PIN3  33  // Arduino pin tied to trigger pin on the ultrasonic sensor.
#define ECHO_PIN3    31  // Arduino pin tied to echo pin on the ultrasonic sensor.
#define MAX_DISTANCE3 400   // NewPing Lib
*/

// KEYPAD
const byte ROWS = 4; //four rows
const byte COLS = 4; //three columns

// 42 - 49
const int pinUp1   = 43; 
const int pinDown1 = 42;
const int pinUp2   = 45;
const int pinDown2 = 44;
const int pinUp3   = 47;
const int pinDown3 = 46;
const int pinUp4   = 49;
const int pinDown4 = 48;


const char keys[ROWS][COLS] = {
  {'A', '1', '2', '3'},
  {'B', '4', '5', '6'},
  {'C', '7', '8', '9'},
  {'D', '*', '0', '#'}
};

// DEFINED IN SKETCH!!
// byte rowPins[ROWS] = {pinDown1, pinDown2, pinDown3, pinDown4}; //connect to the row pinouts of the keypad
// byte colPins[COLS] = {pinUp1, pinUp2, pinUp3, pinUp4}; //connect to the column pinouts of the keypad

// define the pins used
#define CLK 52       // SPI Clock, shared with SD card
#define MISO 50      // Input data, from VS1053/SD card
#define MOSI 51      // Output data, to VS1053/SD card
// Connect CLK, MISO and MOSI to hardware SPI pins.
// See http://arduino.cc/en/Reference/SPI "Connections"

// These are the pins used for the breakout example
#define BREAKOUT_RESET  9 
// XXX In previous version, 15-6, CS was 7 and XDCS was 8
#define BREAKOUT_CS     8    // VS1053 chip select pin (output)
#define BREAKOUT_DCS    7     // VS1053 Data/command select pin (output)
// These are the pins used for the music maker shield
// #define SHIELD_RESET  -1      // VS1053 reset pin (unused!)
// #define SHIELD_CS     7      // VS1053 chip select pin (output)
// #define SHIELD_DCS    6      // VS1053 Data/command select pin (output)

// These are common pins between breakout and shield
#define CARDCS 4     // Card chip select pin
// DREQ should be an Int pin, see http://arduino.cc/en/Reference/attachInterrupt
#define DREQ 18       // VS1053 Data request, ideally an Interrupt pin

#define PRESENCE_DISTANCE 400

class RioPhoneHack { 
public:
	RioPhoneHack();
	~RioPhoneHack();
    void on();
	void off();
	void blink(int time);
	long checkProx();
	String getQuestionFileName(String rioNumber, int questionsCount);
	String getRio3Answer7(int amount);
	String getRio1Answer1(int age);
	String getRio1Answer11();
	String getRio1Answer23(int sex);
	int getRandom(int max);
	bool getRandomRing();
	String getSufix(int question, int amount);
	bool checkPresence(long cm);
};

#endif