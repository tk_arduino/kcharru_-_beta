#include "RioPhoneHack.h" //include the declaration for this class

const byte LED_PIN = 13; //use the LED @ Arduino pin 13




//<<constructor>> setup the LED, make pin 13 an OUTPUT
RioPhoneHack::RioPhoneHack(){
	//Serial.begin (9600);
    pinMode(LED_PIN, OUTPUT); //make that pin an OUTPUT
}

//<<destructor>>
RioPhoneHack::~RioPhoneHack(){/*nothing to destruct*/}

//turn the LED on
void RioPhoneHack::on(){
	Serial.println("desde on lib");
	digitalWrite(LED_PIN,HIGH); //set the pin HIGH and thus turn LED on
}

//turn the LED off
void RioPhoneHack::off(){
	Serial.println("DESDE OFF Lib");
	digitalWrite(LED_PIN,LOW); //set the pin LOW and thus turn LED off
}

//blink the LED in a period equal to paramterer -time.
void RioPhoneHack::blink(int time){
	
	on(); 			//turn LED on
	delay(time/2);  //wait half of the wanted period
	off();			//turn LED off
	delay(time/10);  //wait the last half of the wanted period
}

long duration_ms, cm;
// long inches;

long RioPhoneHack::checkProx(){
  // Serial.println("1 ");
  digitalWrite(trigPin, LOW);

  delayMicroseconds(5);
  // Serial.print("2 ");
  digitalWrite(trigPin, HIGH);
  // Serial.print("3 ");
  delayMicroseconds(10);
  // Serial.print("4 ");
  digitalWrite(trigPin, LOW);
  // Serial.print("5 ");
 
  pinMode(echoPin, INPUT);
  Serial.print("6 ");
  duration_ms = pulseIn(echoPin, HIGH);
  Serial.print("7 ");
 
  cm = (duration_ms/2) / 29.1;
  
  return cm;
  
}

// bool RioPhoneHack::checkPresence(long min){
//   long cm = RioPhoneHack::checkProx();
//   if(cm > min){
//     return false;
//   }
//   else{
//     return true;
//   }
// }
bool RioPhoneHack::checkPresence(long cm){
  if(cm != 0 && cm < PRESENCE_DISTANCE){
    return true;
  }
  else{
    return false;
  }
}

String RioPhoneHack::getQuestionFileName(String rioNumber, int questionsCount) {
  // rioNumber is the folder
  String prefix;
  if (questionsCount < 10) {
    prefix = "00";
  }
  else {
    prefix = "0";
  }

  String name = rioNumber  + prefix + String(questionsCount) + ".mp3";
  return name;
}

String RioPhoneHack::getRio3Answer7(int amount) {
  String answer7;
  if (amount <= 7) {
    answer7 = "007a.mp3";
  }
  else if (amount <= 9) {
    answer7 = "007b.mp3";
  }
  else if (amount <= 20) {
    answer7 = "007c.mp3";
  }
  else {
    answer7 = "007d.mp3";
  }

  return answer7;
}

String RioPhoneHack::getRio1Answer1(int age) {
  String answer;
  if (age >= 14) {
    answer = "OK";
  }
  else if (age == 13) {
    answer = "001a.mp3";
  }
  else if (age == 12) {
    answer = "001b.mp3";
  }
  else if (age == 11) {
    answer = "001c.mp3";
  }
  else if (age == 10) {
    answer = "001d.mp3";
  }
  else if (age == 9) {
    answer = "001e.mp3";
  }
  // 8 and under
  else{
    answer = "001f.mp3";
  }
  

  return answer;
}
String RioPhoneHack::getRio1Answer11() {
  randomSeed(analogRead(A0));
  int randomNumber = random(3);
  Serial.println("randomNumber: " + String(randomNumber));
  String answer;
  switch(randomNumber){
    case 0:
      answer = "011a.mp3";
      break;
    case 1:
      answer = "011b.mp3";
      break;
    case 2:
      answer = "011c.mp3";
      break;

  }
  return answer;
}
int RioPhoneHack::getRandom(int max) {
  randomSeed(analogRead(A0));
  int randomNumber = random(max);
  return randomNumber;
}
bool RioPhoneHack::getRandomRing() {
  randomSeed(analogRead(A0));
  int randomNumber = random(2);
  Serial.println("randomNumber in getRandomRing " + String(randomNumber));
  bool doit;
  if(randomNumber == 0){
    doit = false;
  }
  else{
    doit = true;
  }
  return doit;
}

String RioPhoneHack::getSufix(int question, int amount){
  String sufix = "";
  
  // RioHack #1
  if(question == 1){
    if(amount == 1){
      sufix = " year old";
    }
    else{
      sufix = " years old";
    }
  }
  // RioHack #3
  if(question == 4){
    if(amount == 1){
      sufix = " cloud";
    }
    else{
      sufix = " clouds";
    }
  }
  if(question == 7){
    if(amount == 1){
      sufix = " handle";
    }
    else{
      sufix = " handles";
    }
  }
  return sufix;

}

String RioPhoneHack::getRio1Answer23(int sex) {
  randomSeed(analogRead(A0));
  int randomNumber = random(3);
  Serial.println("randomNumber: " + String(randomNumber));
  Serial.println("sext: " + String(sex));
  String answer;
  switch(randomNumber){
    case 0:
      // Woman
      if(sex == 1){
        answer = "023a.mp3";
      }
      // Man 
      else{
        answer = "023d.mp3";
      }
      
      break;
    case 1:
      // Woman
      if(sex == 1){
        answer = "023b.mp3";
      }
      // Man 
      else{
        answer = "023e.mp3";
      }
      break;
    case 2:
      // Woman
      if(sex == 1){
        answer = "023c.mp3";
      }
      // Man 
      else{
        answer = "023f.mp3";
      }
      break;

  }
  return answer;
}



