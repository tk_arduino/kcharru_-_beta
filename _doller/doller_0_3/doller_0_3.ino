/******************************************************************************

LilyPad Pixel Board - Set Colors Example
Angela Sheehan
SparkFun Electronics

Adapted from SparkFun's WS2812 Breakout Hookup Guide code examples

This code demonstrates setting colors on individual LilyPad Pixel Boards using
the NeoPixel library.

******************************************************************************/

#include <Adafruit_NeoPixel.h>

#define PIN 12  //Which pin the pixels are connected to
#define LED_COUNT 1  //Number of pixels used

// Create an instance of the Adafruit_NeoPixel class called "leds".
// That'll be what we refer to from here on...
Adafruit_NeoPixel leds = Adafruit_NeoPixel(LED_COUNT, PIN, NEO_GRB + NEO_KHZ800);



// Define the number of samples to keep track of. The higher the number, the
// more the readings will be smoothed, but the slower the output will respond to
// the input. Using a constant rather than a normal variable lets us use this
// value to determine the size of the readings array.
const int numReadings = 10;

int readings[numReadings];      // the readings from the analog input
int readIndex = 0;              // the index of the current reading
int total = 0;                  // the running total
int average = 0;                // the average

int inputPin = A0;


byte piezoPin = 7;  

// Pin the led is attached to
int leda = 1;
int ledb = 2;
int ledc = 3;



// these constants describe the pins. They won't change:
const int xpin = A0;                  // x-axis of the accelerometer
const int ypin = A1;                  // y-axis
const int zpin = A2;                  // z-axis (only on 3-axis models)

int xx = 0;
int yy = 0;
int zz = 0;

int sampleDelay = 500;   //number of milliseconds between readings
void setup()
{

   pinMode(piezoPin,OUTPUT);
// Set the led pin as an OUTPUT
    pinMode(leda, OUTPUT);
    pinMode(ledb, OUTPUT);
    pinMode(ledc, OUTPUT);
   
 leds.begin();  // Start up the LED strip
 leds.show();   // LEDs don't actually update until you call this function

   // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);

   for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    readings[thisReading] = 0;
  }


   //Make sure the analog-to-digital converter takes its reference voltage from
 // the AREF pin
 pinMode(xpin, INPUT);
 pinMode(ypin, INPUT);
 pinMode(zpin, INPUT);

 
}

void loop()
{



   int x = analogRead(xpin);
 //
 //add a small delay between pin readings.  I read that you should
 //do this but haven't tested the importance
   delay(1); 
 //
 int y = analogRead(ypin);
 //
 //add a small delay between pin readings.  I read that you should
 //do this but haven't tested the importance
   delay(1); 
 //
 int z = analogRead(zpin);
 //
 //zero_G is the reading we expect from the sensor when it detects
 //no acceleration.  Subtract this value from the sensor reading to
 //get a shifted sensor reading.
 float zero_G =512; 
 //
 //scale is the number of units we expect the sensor reading to
 //change when the acceleration along an axis changes by 1G.
 //Divide the shifted sensor reading by scale to get acceleration in Gs.
 float scale =102.3;
 //
 xx = ((float)x - zero_G)/scale;
 
 Serial.print(((float)x - zero_G)/scale);
 Serial.print("\t");
 //
 
 yy = ((float)y - zero_G)/scale;
 Serial.print(((float)y - zero_G)/scale);
 Serial.print("\t");
 //
 
 zz = ((float)z - zero_G)/scale;
  Serial.print(((float)z - zero_G)/scale);
 Serial.print("\n");
 //
 // delay before next reading:
 delay(sampleDelay);

 if (zz > 0) {

  soundFX(3000.0,30+200*(1+sin(millis()/5000))); // wonky sonic screwdriver
   //soundFX(3000.0,30); // sonic screwdriver
   soundFX(100.0,30.0); // ray gun
   soundFX(3.0,30.0); // star trek
   //soundFX(1.0,30.0); // star trek high
   
  }

  


  // subtract the last reading:
  total = total - readings[readIndex];
  // read from the sensor:
  readings[readIndex] = analogRead(inputPin);
  // add the reading to the total:
  total = total + readings[readIndex];
  // advance to the next position in the array:
  readIndex = readIndex + 1;

  // if we're at the end of the array...
  if (readIndex >= numReadings) {
    // ...wrap around to the beginning:
    readIndex = 0;
  }

  // calculate the average:
  average = total / numReadings;
  // send it to the computer as ASCII digits
  //Serial.println(average);
  // int sensorValue = average;
    int sensorValue = analogRead(inputPin);
  delay(10);       // delay in between reads for stability

  
    // read the input on analog pin 0:
  //int sensorValue = analogRead(A1);

  
  // print out the value you read:

  // sensorValue = sensorValue / 1023.0 * 5.0;
  
 Serial.println(sensorValue);


/**/
 /* if (sensorValue > 0) {
  leds.setPixelColor(0, 0, 255, 0); // Set the first pixel to RED
    leds.setPixelColor(0, 0, round(sensorValue), 0); // Set the first pixel to RED
  leds.show(); //Display the colors
  } else  {
      leds.setPixelColor(0, 0, 0, 0); // Set the first pixel to RED
  leds.show(); //Display the colors
  }
*/
 //   delay(25);        // delay in between reads for stability
 

//  leds.setPixelColor(0, 0, sensorValue, 0); // Set the first pixel to RED
//  leds.show(); //Display the colors
  
 //    delay(50);        // delay in between reads for stability
  /*delay(1000);
  leds.setPixelColor(0, 0, 255, 0); // Set the second pixel to GREEN
leds.show(); //Display the colors
delay(1000);
  leds.setPixelColor(0, 0, 0, 255); // Set the third pixel to BLUE
  leds.show(); //Display the colors
  delay(1000);*/
}

void soundFX(float amplitude,float period){ 
 int uDelay=2+amplitude+amplitude*sin(millis()/period);
 for(int i=0;i<5;i++){
   digitalWrite(piezoPin,HIGH);
   
   digitalWrite(leda,HIGH);
   digitalWrite(ledb,HIGH);
   digitalWrite(ledc,HIGH);

    leds.setPixelColor(0, 255, 0, 0); // Set the first pixel to RED
  leds.show(); //Display the colors
  
   
   delayMicroseconds(uDelay);
   digitalWrite(piezoPin,LOW);
   
   digitalWrite(leda,LOW);
   digitalWrite(ledb,LOW);
   digitalWrite(ledc,LOW);

       leds.setPixelColor(0, 0, 0, 0); // Set the first pixel to RED
  leds.show(); //Display the colors
   
   delayMicroseconds(uDelay);
 }
}
