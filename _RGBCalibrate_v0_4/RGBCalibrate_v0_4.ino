#include "FastLED.h"


////////////////////////////////////////////////////////////////////////////////////////////////////
//
// RGB Calibration code
//
// Use this sketch to determine what the RGB ordering for your chipset should be.  Steps for setting up to use:

// * Uncomment the line in setup that corresponds to the LED chipset that you are using.  (Note that they
//   all explicitly specify the RGB order as RGB)
// * Define DATA_PIN to the pin that data is connected to.
// * (Optional) if using software SPI for chipsets that are SPI based, define CLOCK_PIN to the clock pin
// * Compile/upload/run the sketch

// You should see six leds on.  If the RGB ordering is correct, you should see 1 red led, 2 green
// leds, and 3 blue leds.  If you see different colors, the count of each color tells you what the
// position for that color in the rgb orering should be.  So, for example, if you see 1 Blue, and 2
// Red, and 3 Green leds then the rgb ordering should be BRG (Blue, Red, Green).

// You can then test this ordering by setting the RGB ordering in the addLeds line below to the new ordering
// and it should come out correctly, 1 red, 2 green, and 3 blue.
//
//////////////////////////////////////////////////

#define NUM_LEDS 19

// Data pin that led data will be written out over
#define DATA_PIN 3
// Clock pin only needed for SPI based chipsets when not using hardware SPI
#define CLOCK_PIN 13

CRGB leds[NUM_LEDS];


int potPin1 = 14;    // select the input pin for the potentiometer
int potPin2 = 19;    // select the input pin for the potentiometer
int potValue1 = analogRead(potPin1);;    // variable to store the value coming from the sensor
int potValue2 = analogRead(potPin2);;    // variable to store the value coming from the sensor
 int ledValue1 = 0;
 int ledValue2 = 0;

void setup() {
    // sanity check delay - allows reprogramming if accidently blowing power w/leds
    delay(2000);


     FastLED.addLeds<WS2801, DATA_PIN, CLOCK_PIN, RGB>(leds, NUM_LEDS);
     
     
     
           // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  
}

void loop() {
  
  ledValue1 = analogRead(A2);
      // initialize serial communication at 9600 bit
//    Serial.print("ledValue1: ");
 // Serial.print(ledValue1);
//  Serial.println();
  
   ledValue2 = analogRead(A3);
      // initialize serial communication at 9600 bit
//    Serial.print("ledValue2: ");
//  Serial.print(ledValue2);
//  Serial.println();

  potValue1 = analogRead(potPin1);    // read the value from the sensor
//     Serial.print("potValue1: ");
//  Serial.print(potValue1);
//  Serial.println();

    potValue2 = analogRead(potPin2);    // read the value from the sensor
//     Serial.print("potValue2: ");
//  Serial.print(potValue2);
//  Serial.println();
         
  if (ledValue1 > potValue1) {          
    for (int j=0; j<10; j=j+1) { 
      int ranNum = random(255);
      leds[j] = CRGB(0,5,0);
    } 
    //digitalWrite(7,HIGH);
    FastLED.show();
    
    delay(100);
    
    //digitalWrite(7,LOW);
    for (int k=0; k<10; k=k+1) { 
      //int ranNum = random(255);
      leds[k] = CRGB(0,0,0);
    } 
    
    FastLED.show();
    
  } else if (ledValue2 > potValue2) {          
    for (int l=10; l<NUM_LEDS; l=l+1) { 
      //int ranNum = random(255);
      leds[l] = CRGB(255,0,0);
    } 
    //digitalWrite(7,HIGH);
    
    FastLED.show();
    
    //digitalWrite(7,LOW);
    
    delay(100);
    for (int k=10; k<NUM_LEDS; k=k+1) { 
      //int ranNum = random(255);
      leds[k] = CRGB(0,0,0);
    } 
    
    FastLED.show();
    
  } 

}


